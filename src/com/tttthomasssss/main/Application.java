package com.tttthomasssss.main;

import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.tttthomasssss.controllers.*;
import com.tttthomasssss.defaults.CheckersDefaults.PieceColour;
import com.tttthomasssss.models.CheckersBoardState;
import com.tttthomasssss.models.CheckersModel;
import com.tttthomasssss.views.CheckersMainView;

public class Application 
implements Serializable
{

	private static final long serialVersionUID = -3415431271228532510L;

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		 /*
         * In case the X-Platform LnF couldn't be set (which should be quite impossible),
         * the System automatically falls back on the default, so we actually have 
         * nothing to do in the Exception Handlers
         */
		
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
                e.printStackTrace();
        } catch (InstantiationException e) {
                e.printStackTrace();
        } catch (IllegalAccessException e) {
                e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
                e.printStackTrace();
        }
		
		Application app = new Application();
		
		boolean isApplet = false;
		if (args.length > 0) {
			isApplet = args[0].equals("applet");
		}
		
		app.startGUI(isApplet);
		
		//app.startAlphaTest(20);
		//app.startAlpha();
		//app.startAlphaBetaTest(20);
		//app.startTest(10);
		//app.startMinimaxTest(10);
		//app.startCommandLineSimulation();
		//app.startMiniMaxCmdSim();
		//app.startAlphaBeta();
	}
	
	private void startGUI(boolean isApplet)
	{
		CheckersController controller = new CheckersController(this, isApplet);
		CheckersModel model = new CheckersModel(controller, PieceColour.WHITE);
		CheckersMainView view = new CheckersMainView(controller);
		
		controller.setModel(model);
		controller.setView(view);
		
		controller.initGame();
		
		if (!isApplet) {
			controller.getView().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		controller.getView().setVisible(true);
	}
	
	private void startMiniMaxCmdSim()
	{
		CheckersController controller = new CheckersController(this, false);
			
		PieceColour player = PieceColour.WHITE;
		PieceColour currPlayer = PieceColour.WHITE;
		//CheckersBoardState model = new CheckersBoardState();
		CheckersModel model = new CheckersModel(controller, currPlayer);
			
		controller.setModel(model);
			
		controller.getModel().printCurrentState();
		System.out.println();
			
		//controller.getModel().printCurrentScore();
			
		CheckersBoardState state = controller.getModel().getCurrentState();
		ArrayList<CheckersBoardState> bestStates = new ArrayList<CheckersBoardState>();
		int alpha = Integer.MIN_VALUE + 1;
		int beta = Integer.MAX_VALUE;
		while (!controller.getModel().isTerminalState(state)) {
			//state.resetMoveSequence();
			ArrayList<CheckersBoardState> successors = state.getSuccessors(currPlayer, true);
			int minimax = (currPlayer == player) ? Integer.MIN_VALUE : Integer.MAX_VALUE;
			boolean maximise = currPlayer == player;
			bestStates.clear();
			//currPlayer = PieceColour.flipColour(currPlayer);
			for (CheckersBoardState s : successors) {
				int score = controller.getModel().minimaxEvaluation2(currPlayer, s, 1, true);
				
				// State Selection
				if (score == minimax || currPlayer != player) {
					bestStates.add(s);
				} else if ((maximise && score > minimax) || (!maximise && score < minimax)) {
					bestStates.clear();
					bestStates.add(s);
					minimax = score;
						
					// Alpha-Beta Pruning bookkeeping
					if (currPlayer == player) {
						alpha = score;
					} else {
						beta = score;
					}
				}
			}
				
			System.out.println("EXPLORED " + controller.getModel().getExploreCount() + " POSITIONS!");
			controller.getModel().resetExploreCount();
				
			if (successors.size() <= 0) { // Draw
				System.out.println("No More Successors!");
				break;
			} else {
				System.out.println("State after "  + currPlayer.toString() + " moved!");
				int index = (int)(Math.random() * bestStates.size());
				state = bestStates.get(index);
				state.printGameBoard();
				System.out.println("MOVE SEQ: " + state.getMoveSequence());
				currPlayer = PieceColour.flipColour(currPlayer);
			}
				
		}
			
		System.out.println("END OF GAME!");
		System.out.println("FINAL STATE");
		state.printGameBoard();
		System.out.println("WON FOR WHITE: " + state.isWinningStateForColour(PieceColour.WHITE));
		System.out.println("WON FOR BLACK: " + state.isWinningStateForColour(PieceColour.BLACK));
		state.printRemainingPieceNumbers();
			
	}
	
	private void startCommandLineSimulation()
	{
		CheckersController controller = new CheckersController(this, false);
		
		PieceColour player = PieceColour.WHITE;
		PieceColour currPlayer = PieceColour.WHITE;
		//CheckersBoardState model = new CheckersBoardState();
		CheckersModel model = new CheckersModel(controller, currPlayer);
		
		controller.setModel(model);
		
		controller.getModel().printCurrentState();
		System.out.println();
		
		//controller.getModel().printCurrentScore();
		
		CheckersBoardState state = controller.getModel().getCurrentState();
		ArrayList<CheckersBoardState> bestStates = new ArrayList<CheckersBoardState>();
		int alpha = Integer.MIN_VALUE + 1;
		int beta = Integer.MAX_VALUE;
		while (!controller.getModel().isTerminalState(state)) {
			//state.resetMoveSequence();
			ArrayList<CheckersBoardState> successors = state.getSuccessors(currPlayer, true);
			int minimax = (currPlayer == player) ? Integer.MIN_VALUE : Integer.MAX_VALUE;
			boolean maximise = currPlayer == player;
			bestStates.clear();
			//currPlayer = PieceColour.flipColour(currPlayer);
			for (CheckersBoardState s : successors) {
				int score = controller.getModel().minimaxEvaluation(currPlayer, s, 1, true);
				
				// State Selection
				if (score == minimax || currPlayer != player) {
					bestStates.add(s);
				} else if ((maximise && score > minimax) || (!maximise && score < minimax)) {
					bestStates.clear();
					bestStates.add(s);
					minimax = score;
						
					// Alpha-Beta Pruning bookkeeping
					if (currPlayer == player) {
						alpha = score;
					} else {
						beta = score;
					}
				}
			}
				
			System.out.println("EXPLORED " + controller.getModel().getExploreCount() + " POSITIONS!");
			controller.getModel().resetExploreCount();
				
			if (successors.size() <= 0) { // Draw
				System.out.println("No More Successors!");
				break;
			} else {
				System.out.println("State after "  + currPlayer.toString() + " moved!");
				int index = (int)(Math.random() * bestStates.size());
				state = bestStates.get(index);
				state.printGameBoard();
				System.out.println("MOVE SEQ: " + state.getMoveSequence());
				currPlayer = PieceColour.flipColour(currPlayer);
			}
				
		}
		
		System.out.println("END OF GAME!");
		System.out.println("FINAL STATE");
		state.printGameBoard();
		System.out.println("WON FOR WHITE: " + state.isWinningStateForColour(PieceColour.WHITE));
		System.out.println("WON FOR BLACK: " + state.isWinningStateForColour(PieceColour.BLACK));
		state.printRemainingPieceNumbers();
		
		
		//controller.getModel().minimaxEvaluation();
		
		// King conversion move
		// White
		/*
		CheckersPosition from = new CheckersPosition(1, 0);
		CheckersPosition to = new CheckersPosition(0, 1);
		controller.getModel().movePiece(from, to);
		System.out.println();
		controller.getModel().printCurrentState();
		// Black
		from = new CheckersPosition(6, 1);
		to = new CheckersPosition(7, 0);
		controller.getModel().movePiece(from, to);
		System.out.println();
		controller.getModel().printCurrentState();
		*/
		 /* // Single Capture move
		CheckersPosition from = new CheckersPosition(7, 2);
		CheckersPosition to = new CheckersPosition(5, 4);
		controller.getModel().movePiece(from, to);
		System.out.println();
		controller.getModel().printCurrentState();
		*/
		/*
		ArrayList<CheckersBoardState> s = controller.getModel().getSuccessors(PieceColour.WHITE);
		
		for (CheckersBoardState state : s) {
			System.out.println("-------------");
			state.printGameBoard();
		}
		*/
		/*
		// Do a dummy move for black
		CheckersPosition oldPos = new CheckersPosition(1, 1);
		CheckersPosition newPos = new CheckersPosition(2, 2);
		controller.movePiece(oldPos, newPos);
		
		// Do a dummy move for white
		oldPos = new CheckersPosition(6, 1);
		newPos = new CheckersPosition(5, 0);
		controller.movePiece(oldPos, newPos);
		*/
	}
	
	private void startMinimaxTest(int nrOfTests)
	{
		CheckersController controller = new CheckersController(this, false);
		
		PieceColour player = PieceColour.WHITE;
		PieceColour currPlayer = PieceColour.WHITE;
		
		int whiteWins = 0;
		int blackWins = 0;
		int draws = 0;
		
		for (int i = 0; i < nrOfTests; i++) {
		
			CheckersModel model = new CheckersModel(controller, currPlayer);
			
			controller.setModel(model);
			
			controller.getModel().printCurrentState();
			System.out.println();
			
			//controller.getModel().printCurrentScore();
			
			CheckersBoardState state = controller.getModel().getCurrentState();
			ArrayList<CheckersBoardState> bestStates = new ArrayList<CheckersBoardState>();
			int alpha = Integer.MIN_VALUE + 1;
			int beta = Integer.MAX_VALUE;
			while (!controller.getModel().isTerminalState(state)) {
				//state.resetMoveSequence();
				ArrayList<CheckersBoardState> successors = state.getSuccessors(currPlayer, true);
				int max = Integer.MIN_VALUE;
				bestStates.clear();
				//currPlayer = PieceColour.flipColour(currPlayer);
				for (CheckersBoardState s : successors) {
					int score = controller.getModel().minimaxEvaluation2(currPlayer, s, 1, true);
					//int score = controller.getModel().minimaxEvaluation(currPlayer, s, 1, true);
					
					// State Selection
					if (score == max || currPlayer != player) {
						bestStates.add(s);
					} else if (score > max) {
						bestStates.clear();
						bestStates.add(s);
						max = score;
						
						// Alpha-Beta Pruning bookkeeping
						if (currPlayer == player) {
							alpha = score;
						} else {
							beta = score;
						}
					}
				}
				
				System.out.println("EXPLORED " + controller.getModel().getExploreCount() + " POSITIONS!");
				controller.getModel().resetExploreCount();
				
				if (successors.size() <= 0) { // Draw
					System.out.println("No More Successors!");
					break;
				} else {
					System.out.println("State after "  + currPlayer.toString() + " moved!");
					int index = (int)(Math.random() * bestStates.size());
					state = bestStates.get(index);
					state.printGameBoard();
					System.out.println("MOVE SEQ: " + state.getMoveSequence());
					currPlayer = PieceColour.flipColour(currPlayer);
				}
				
			}
		
			System.out.println("END OF GAME!");
			System.out.println("FINAL STATE");
			state.printGameBoard();
			System.out.println("WON FOR WHITE: " + state.isWinningStateForColour(PieceColour.WHITE));
			System.out.println("WON FOR BLACK: " + state.isWinningStateForColour(PieceColour.BLACK));
			state.printRemainingPieceNumbers();
			
			if (state.isWinningStateForColour(PieceColour.WHITE) && !state.isWinningStateForColour(PieceColour.BLACK)) {
				whiteWins++;
			} else if (state.isWinningStateForColour(PieceColour.BLACK) && !state.isWinningStateForColour(PieceColour.WHITE)) {
				blackWins++;
			} else {
				draws++;
			}
			
			System.out.println("WHITE WINS: " + whiteWins + " OUT OF " + nrOfTests + " GAMES!");
			System.out.println("BLACK WINS: " + blackWins + " OUT OF " + nrOfTests + " GAMES!");
			System.out.println(draws + " DRAWS OUT OF " + nrOfTests + " GAMES!");
		}
	}
	
	private void startTest(int nrOfTests)
	{
		CheckersController controller = new CheckersController(this, false);
		
		PieceColour player = PieceColour.WHITE;
		PieceColour currPlayer = PieceColour.WHITE;
		
		int whiteWins = 0;
		int blackWins = 0;
		int draws = 0;
		
		for (int i = 0; i < nrOfTests; i++) {
		
			CheckersModel model = new CheckersModel(controller, currPlayer);
			
			controller.setModel(model);
			
			controller.getModel().printCurrentState();
			System.out.println();
			
			//controller.getModel().printCurrentScore();
			
			CheckersBoardState state = controller.getModel().getCurrentState();
			ArrayList<CheckersBoardState> bestStates = new ArrayList<CheckersBoardState>();
			int alpha = Integer.MIN_VALUE + 1;
			int beta = Integer.MAX_VALUE;
			while (!controller.getModel().isTerminalState(state)) {
				//state.resetMoveSequence();
				ArrayList<CheckersBoardState> successors = state.getSuccessors(currPlayer, true);
				int max = Integer.MIN_VALUE;
				bestStates.clear();
				//currPlayer = PieceColour.flipColour(currPlayer);
				for (CheckersBoardState s : successors) {
					//int score = controller.getModel().minimaxEvaluation2(currPlayer, s, 1, true);
					int score = controller.getModel().minimaxEvaluation(currPlayer, s, 1, true);
					
					// State Selection
					if (score == max || currPlayer != player) {
						bestStates.add(s);
					} else if (score > max) {
						bestStates.clear();
						bestStates.add(s);
						max = score;
						
						// Alpha-Beta Pruning bookkeeping
						if (currPlayer == player) {
							alpha = score;
						} else {
							beta = score;
						}
					}
				}
				
				System.out.println("EXPLORED " + controller.getModel().getExploreCount() + " POSITIONS!");
				controller.getModel().resetExploreCount();
				
				if (successors.size() <= 0) { // Draw
					System.out.println("No More Successors!");
					break;
				} else {
					System.out.println("State after "  + currPlayer.toString() + " moved!");
					int index = (int)(Math.random() * bestStates.size());
					state = bestStates.get(index);
					state.printGameBoard();
					System.out.println("MOVE SEQ: " + state.getMoveSequence());
					currPlayer = PieceColour.flipColour(currPlayer);
				}
				
			}
		
			System.out.println("END OF GAME!");
			System.out.println("FINAL STATE");
			state.printGameBoard();
			System.out.println("WON FOR WHITE: " + state.isWinningStateForColour(PieceColour.WHITE));
			System.out.println("WON FOR BLACK: " + state.isWinningStateForColour(PieceColour.BLACK));
			state.printRemainingPieceNumbers();
			
			if (state.isWinningStateForColour(PieceColour.WHITE) && !state.isWinningStateForColour(PieceColour.BLACK)) {
				whiteWins++;
			} else if (state.isWinningStateForColour(PieceColour.BLACK) && !state.isWinningStateForColour(PieceColour.WHITE)) {
				blackWins++;
			} else {
				draws++;
			}
			
			System.out.println("WHITE WINS: " + whiteWins + " OUT OF " + nrOfTests + " GAMES!");
			System.out.println("BLACK WINS: " + blackWins + " OUT OF " + nrOfTests + " GAMES!");
			System.out.println(draws + " DRAWS OUT OF " + nrOfTests + " GAMES!");
		}
	}
	
	private void startAlphaBetaTest(int nrOfTests)
	{
		CheckersController controller = new CheckersController(this, false);
		
		PieceColour player = PieceColour.WHITE;
		PieceColour currPlayer = PieceColour.WHITE;
		
		int whiteWins = 0;
		int blackWins = 0;
		int draws = 0;
		
		for (int i = 0; i < nrOfTests; i++) {
		
			CheckersModel model = new CheckersModel(controller, currPlayer);
			
			controller.setModel(model);
			
			controller.getModel().printCurrentState();
			System.out.println();
			
			//controller.getModel().printCurrentScore();
			
			CheckersBoardState state = controller.getModel().getCurrentState();
			ArrayList<CheckersBoardState> bestStates = new ArrayList<CheckersBoardState>();
			int alpha = Integer.MIN_VALUE + 1;
			int beta = Integer.MAX_VALUE;
			while (!controller.getModel().isTerminalState(state)) {
				ArrayList<CheckersBoardState> successors = state.getSuccessors(currPlayer, true);
				int max = Integer.MIN_VALUE;
				bestStates.clear();
				//currPlayer = PieceColour.flipColour(currPlayer);
				for (CheckersBoardState s : successors) {
					int eval = controller.getModel().alphabetaEvaluation(currPlayer, s, 1, -beta, -alpha, true);
					
					// State Selection
					if (eval == max || currPlayer != player) {
						bestStates.add(s);
					} else if (eval > max) {
						bestStates.clear();
						bestStates.add(s);
						max = eval;
						
						// Alpha-Beta Pruning bookkeeping
						if (currPlayer == player) {
							//beta = -alpha;
							alpha = eval;
						} else {
							//alpha = -beta;
							beta = eval;
							//alpha = -eval;
						}
					}
				}
				
				System.out.println("EXPLORED " + controller.getModel().getExploreCount() + " POSITIONS!");
				controller.getModel().resetExploreCount();
				
				if (successors.size() <= 0) { // Draw
					System.out.println("No More Successors!");
					break;
				} else {
					System.out.println("State after "  + currPlayer.toString() + " moved!");
					int index = (int)(Math.random() * bestStates.size());
					state = bestStates.get(index);
					state.printGameBoard();
					System.out.println("MOVE SEQ: " + state.getMoveSequence());
					currPlayer = PieceColour.flipColour(currPlayer);
				}
				
			}
		
			System.out.println("END OF GAME!");
			System.out.println("FINAL STATE");
			state.printGameBoard();
			System.out.println("WON FOR WHITE: " + state.isWinningStateForColour(PieceColour.WHITE));
			System.out.println("WON FOR BLACK: " + state.isWinningStateForColour(PieceColour.BLACK));
			state.printRemainingPieceNumbers();
			
			if (state.isWinningStateForColour(PieceColour.WHITE) && !state.isWinningStateForColour(PieceColour.BLACK)) {
				whiteWins++;
			} else if (state.isWinningStateForColour(PieceColour.BLACK) && !state.isWinningStateForColour(PieceColour.WHITE)) {
				blackWins++;
			} else {
				draws++;
			}
			
			System.out.println("WHITE WINS: " + whiteWins + " OUT OF " + nrOfTests + " GAMES!");
			System.out.println("BLACK WINS: " + blackWins + " OUT OF " + nrOfTests + " GAMES!");
			System.out.println(draws + " DRAWS OUT OF " + nrOfTests + " GAMES!");
		}
	}
	
	// TODO: Set up Alpha Beta Pruning so the second branch of the GameTree is better than the first, but the third branch is worse
	private void startAlphaBeta()
	{
		CheckersController controller = new CheckersController(this, false);
		
		PieceColour player = PieceColour.WHITE;
		PieceColour currPlayer = PieceColour.WHITE;
		//CheckersBoardState model = new CheckersBoardState();
		CheckersModel model = new CheckersModel(controller, currPlayer);
		
		controller.setModel(model);
		
		controller.getModel().printCurrentState();
		System.out.println();
		
		//controller.getModel().printCurrentScore();
		
		CheckersBoardState state = controller.getModel().getCurrentState();
		ArrayList<CheckersBoardState> bestStates = new ArrayList<CheckersBoardState>();
		int alpha = Integer.MIN_VALUE + 1;
		int beta = Integer.MAX_VALUE;
		
		while (!controller.getModel().isTerminalState(state)) {
			ArrayList<CheckersBoardState> successors = state.getSuccessors(currPlayer, true);
			int max = Integer.MIN_VALUE;
			bestStates.clear();
			//currPlayer = PieceColour.flipColour(currPlayer);
			for (CheckersBoardState s : successors) {
				int eval = controller.getModel().alphabetaEvaluation(currPlayer, s, 1, -beta, -alpha, true);
				
				// State Selection
				if (eval == max || currPlayer != player) {
					bestStates.add(s);
				} else if (eval > max) {
					bestStates.clear();
					bestStates.add(s);
					max = eval;
					
					// Alpha-Beta Pruning bookkeeping
					if (currPlayer == player) {
						//beta = -alpha;
						alpha = eval;
					} else {
						//alpha = -beta;
						beta = eval;
						//alpha = -eval;
					}
				}
			}
			
			System.out.println("EXPLORED " + controller.getModel().getExploreCount() + " POSITIONS!");
			controller.getModel().resetExploreCount();
			
			if (successors.size() <= 0) { // Draw
				System.out.println("No More Successors!");
				break;
			} else {
				System.out.println("State after "  + currPlayer.toString() + " moved!");
				int index = (int)(Math.random() * bestStates.size());
				state = bestStates.get(index);
				state.printGameBoard();
				currPlayer = PieceColour.flipColour(currPlayer);
			}
			
		}
		
		System.out.println("END OF GAME!");
		System.out.println("FINAL STATE");
		state.printGameBoard();
		System.out.println("WON FOR WHITE: " + state.isWinningStateForColour(PieceColour.WHITE));
		System.out.println("WON FOR BLACK: " + state.isWinningStateForColour(PieceColour.BLACK));
		state.printRemainingPieceNumbers();
	}
	
	private void startAlpha()
	{
		CheckersController controller = new CheckersController(this, false);
		
		PieceColour player = PieceColour.WHITE;
		PieceColour currPlayer = PieceColour.WHITE;
		//CheckersBoardState model = new CheckersBoardState();
		CheckersModel model = new CheckersModel(controller, currPlayer);
		
		controller.setModel(model);
		
		controller.getModel().printCurrentState();
		System.out.println();
		
		//controller.getModel().printCurrentScore();
		
		CheckersBoardState state = controller.getModel().getCurrentState();
		ArrayList<CheckersBoardState> bestStates = new ArrayList<CheckersBoardState>();
		int alpha = Integer.MIN_VALUE + 1;
		
		while (!controller.getModel().isTerminalState(state)) {
			ArrayList<CheckersBoardState> successors = state.getSuccessors(currPlayer, true);
			int max = Integer.MIN_VALUE;
			bestStates.clear();
			//currPlayer = PieceColour.flipColour(currPlayer);
			for (CheckersBoardState s : successors) {
				int eval = controller.getModel().alphaEvaluation(currPlayer, s, 1, -alpha, true);
				
				// State Selection
				if (eval == max || currPlayer != player) {
					bestStates.add(s);
				} else if (eval > max) {
					bestStates.clear();
					bestStates.add(s);
					max = eval;
					alpha = -eval;
					
					/*
					// Alpha-Beta Pruning bookkeeping
					if (currPlayer == player) {
						//beta = -alpha;
						alpha = eval;
					} else {
						alpha = -eval;
					}
					*/
				}
			}
			
			System.out.println("EXPLORED " + controller.getModel().getExploreCount() + " POSITIONS!");
			controller.getModel().resetExploreCount();
			
			if (successors.size() <= 0) { // Draw
				System.out.println("No More Successors!");
				break;
			} else {
				System.out.println("State after "  + currPlayer.toString() + " moved!");
				int index = (int)(Math.random() * bestStates.size());
				state = bestStates.get(index);
				state.printGameBoard();
				currPlayer = PieceColour.flipColour(currPlayer);
			}
			
		}
		
		System.out.println("END OF GAME!");
		System.out.println("FINAL STATE");
		state.printGameBoard();
		System.out.println("WON FOR WHITE: " + state.isWinningStateForColour(PieceColour.WHITE));
		System.out.println("WON FOR BLACK: " + state.isWinningStateForColour(PieceColour.BLACK));
		state.printRemainingPieceNumbers();
	}
	
	private void startAlphaTest(int nrOfTests)
	{
		CheckersController controller = new CheckersController(this, false);
		
		PieceColour player = PieceColour.WHITE;
		PieceColour currPlayer = PieceColour.WHITE;
		
		int whiteWins = 0;
		int blackWins = 0;
		int draws = 0;
		
		for (int i = 0; i < nrOfTests; i++) {
		
			CheckersModel model = new CheckersModel(controller, currPlayer);
			
			controller.setModel(model);
			
			controller.getModel().printCurrentState();
			System.out.println();
			
			//controller.getModel().printCurrentScore();
			
			CheckersBoardState state = controller.getModel().getCurrentState();
			ArrayList<CheckersBoardState> bestStates = new ArrayList<CheckersBoardState>();
			int alpha = Integer.MIN_VALUE + 1;
			while (!controller.getModel().isTerminalState(state)) {
				ArrayList<CheckersBoardState> successors = state.getSuccessors(currPlayer, true);
				int max = Integer.MIN_VALUE;
				bestStates.clear();
				//currPlayer = PieceColour.flipColour(currPlayer);
				for (CheckersBoardState s : successors) {
					int eval = controller.getModel().alphaEvaluation(currPlayer, s, 1, alpha, true);
					
					// State Selection
					if (eval == max || currPlayer != player) {
						bestStates.add(s);
					} else if (eval > max) {
						bestStates.clear();
						bestStates.add(s);
						max = eval;
						alpha = -eval;
						
						/*
						// Alpha-Beta Pruning bookkeeping
						if (currPlayer == player) {
							//beta = -alpha;
							alpha = eval;
						} else {
							alpha = -eval;
						}*/
					}
				}
				
				System.out.println("EXPLORED " + controller.getModel().getExploreCount() + " POSITIONS!");
				controller.getModel().resetExploreCount();
				
				if (successors.size() <= 0) { // Draw
					System.out.println("No More Successors!");
					break;
				} else {
					System.out.println("State after "  + currPlayer.toString() + " moved!");
					int index = (int)(Math.random() * bestStates.size());
					state = bestStates.get(index);
					state.printGameBoard();
					System.out.println("MOVE SEQ: " + state.getMoveSequence());
					currPlayer = PieceColour.flipColour(currPlayer);
				}
				
			}
		
			System.out.println("END OF GAME!");
			System.out.println("FINAL STATE");
			state.printGameBoard();
			System.out.println("WON FOR WHITE: " + state.isWinningStateForColour(PieceColour.WHITE));
			System.out.println("WON FOR BLACK: " + state.isWinningStateForColour(PieceColour.BLACK));
			state.printRemainingPieceNumbers();
			
			if (state.isWinningStateForColour(PieceColour.WHITE) && !state.isWinningStateForColour(PieceColour.BLACK)) {
				whiteWins++;
			} else if (state.isWinningStateForColour(PieceColour.BLACK) && !state.isWinningStateForColour(PieceColour.WHITE)) {
				blackWins++;
			} else {
				draws++;
			}
			
			System.out.println("WHITE WINS: " + whiteWins + " OUT OF " + nrOfTests + " GAMES!");
			System.out.println("BLACK WINS: " + blackWins + " OUT OF " + nrOfTests + " GAMES!");
			System.out.println(draws + " DRAWS OUT OF " + nrOfTests + " GAMES!");
		}
	}
	
	public void exit(int exitCode)
	{
		// TODO: Delegates App will exit
		System.exit(exitCode);
	}

}
