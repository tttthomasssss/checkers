package com.tttthomasssss.main;

import javax.swing.JApplet;

public class CheckersApplet extends JApplet {

	private static final long serialVersionUID = -2914374452418008697L;
	
	@Override
	public void start()
	{
		String[] args = {"applet"};
		Application.main(args);
	}
	
}
