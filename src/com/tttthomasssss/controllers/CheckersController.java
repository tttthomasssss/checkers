package com.tttthomasssss.controllers;

import java.io.Serializable;
import java.util.prefs.Preferences;

import com.tttthomasssss.main.Application;
import com.tttthomasssss.models.CheckersModel;
import com.tttthomasssss.models.CheckersMoveSequence;
import com.tttthomasssss.models.CheckersPosition;
import com.tttthomasssss.models.CheckersSkynetMove;
import com.tttthomasssss.views.CheckersMainView;
import com.tttthomasssss.defaults.CheckersDefaults;
import com.tttthomasssss.defaults.CheckersDefaults.PieceColour;
import com.tttthomasssss.defaults.CheckersUtils;

public class CheckersController
implements Serializable
{
	
	private static final long serialVersionUID = 1407178570412950101L;
	
	private CheckersModel model;
	private CheckersMainView view;
	private Application application;
	
	private boolean forceCaptures;
	private PieceColour playerColour;
	private int difficulty;
	private boolean alphaBetaPruning;
	
	private boolean activeGame;
	private PieceColour turn;
	
	private boolean applet;
	
	public CheckersController(Application app, boolean isApplet)
	{
		this.applet = isApplet;
		this.application = app;
		
		this.loadPreferences();
		
		// Check if user is white, if so he starts, otherwise the computer does
	}

	public boolean isApplet()
	{
		return this.applet;
	}
	
	public boolean isActiveGame()
	{
		return this.activeGame;
	}
	
	public void activateGame()
	{
		this.activeGame = true;
	}
	
	public void endGame()
	{
		this.activeGame = false;
	}
	
	public PieceColour getMovingPlayer()
	{
		return this.turn;
	}
	
	public boolean isForceCaptures()
	{
		return this.forceCaptures;
	}
	
	public void setForceCaptures(boolean forceCaptures)
	{
		this.forceCaptures = forceCaptures;
		this.storePreferences();
	}
	
	public PieceColour getPlayerColour()
	{
		return this.playerColour;
	}
	
	public void setPlayerColour(PieceColour playerColour)
	{
		this.playerColour = playerColour;
		this.model.setPlayerColour(this.playerColour);
		this.storePreferences();
	}
	
	public int getDifficulty()
	{
		return this.difficulty;
	}
	
	public void setDifficulty(int difficulty)
	{
		this.difficulty = CheckersUtils.sanitiseDifficulty(difficulty);
		this.model.setMaxSearchDepth(CheckersUtils.getMaxSearchDepthForDifficultyAndPruningOption(this.difficulty, this.alphaBetaPruning));
		this.storePreferences();
	}
	
	public boolean isAlphaBetaPruning()
	{
		return this.alphaBetaPruning;
	}
	
	public void setAlphaBetaPruning(boolean alphaBetaPruning)
	{
		this.alphaBetaPruning = alphaBetaPruning;
		this.model.setMaxSearchDepth(CheckersUtils.getMaxSearchDepthForDifficultyAndPruningOption(this.difficulty, this.alphaBetaPruning));
		this.storePreferences();
	}
	
	public boolean movePiece(CheckersPosition from, CheckersPosition to)
	{
		return this.model.movePiece(from, to, this.forceCaptures);
	}
	
	public void capturePieceViewAt(CheckersPosition pos)
	{
		this.view.capturePieceAt(pos);
	}
	
	public void promotePieceViewToKingForMove(CheckersPosition from, CheckersPosition to)
	{
		this.view.promotePieceToKingForMove(from, to);
	}
	
	public void updatePieceCount(int whitePiecesLeft, int blackPiecesLeft)
	{
		this.view.updatePieceCount(whitePiecesLeft, blackPiecesLeft);
	}
	
	public void updateExploreCount(int exploreCount)
	{
		this.view.updateExploreCount(exploreCount);
	}
	
	public void quit(int exitCode)
	{
		this.storePreferences();
		
		this.application.exit(exitCode);
	}
	
	public void resetGame()
	{
		this.endGame();
		this.model.resetState(this.playerColour);
		this.initGame();
	}
	
	public void startGame()
	{
		this.activateGame();
		this.model.setMaxSearchDepth(CheckersUtils.getMaxSearchDepthForDifficultyAndPruningOption(this.difficulty, this.alphaBetaPruning));
		this.view.setPlayerMoveFinished(false);
		
		if (this.turn != this.playerColour) {
			this.skynetsTurn();
		}
	}
	
	public void initGame()
	{
		// Reset the turn and activeGame Vars & the model state
		// Display the current state
		this.model.setPlayerColour(this.playerColour);
		this.view.displayGameState(this.model.getCurrentState());
		this.turn = PieceColour.BLACK;
		
		if (this.turn != this.playerColour) {
			this.view.togglePiecesEnabled(false, null);
		}
		
		//this.view.updateMovingPlayerLabel(this.turn);
		
		/*
		if (this.turn != this.playerColour) {
			this.activateGame();
			this.view.activateGame();
			this.view.toggleViewEnabled(false);
			
			this.skynetsTurn();
			//CheckersMoveSequence skynetsMove = this.skynetsTurn();
			
			
		}*/
	}
	
	public CheckersModel getModel() 
	{
		return this.model;
	}

	public void setModel(CheckersModel model) 
	{
		this.model = model;
	}
	
	public CheckersMainView getView()
	{
		return this.view;
	}
	
	public void setView(CheckersMainView view)
	{
		this.view = view;
	}

	public Application getApplication() 
	{
		return application;
	}
	
	public void flipMovingPlayer()
	{
		this.turn = PieceColour.flipColour(this.turn);
		this.view.updateMovingPlayerLabel(this.turn);
	}
	
	public boolean hasPlayerOpenCapturesFromPosition(CheckersPosition pos)
	{
		return this.model.hasPlayerOpenCapturesFromPosition(pos);
	}
	
	private void loadPreferences()
	{
		// Load from Prefs if the App is executed as a normal Java Application, otherwise just load the defaults
		if (!this.applet) {
			Preferences prefs = Preferences.userNodeForPackage(this.getClass());
			
			this.forceCaptures 		= prefs.getBoolean(CheckersDefaults.PREFS_FORCE_CAPTURES_KEY, CheckersDefaults.PREFS_FORCE_CAPTURES_DEFAULT_VALUE);
			this.playerColour 		= PieceColour.colourForValue(prefs.getInt(CheckersDefaults.PREFS_PLAYER_COLOUR_KEY, CheckersDefaults.PREFS_PLAYER_COLOUR_DEFAULT_VALUE));
			this.difficulty 		= CheckersUtils.sanitiseDifficulty(prefs.getInt(CheckersDefaults.PREFS_DIFFICULTY_KEY, CheckersDefaults.PREFS_DIFFICULTY_DEFAULT_VALUE));
			this.alphaBetaPruning	= prefs.getBoolean(CheckersDefaults.PREFS_ALPHA_BETA_PRUNING_KEY, CheckersDefaults.PREFS_ALPHA_BETA_PRUNING_DEFAULT_VALUE);
		} else {
			this.forceCaptures		= CheckersDefaults.PREFS_FORCE_CAPTURES_DEFAULT_VALUE;
			this.playerColour		= PieceColour.colourForValue(CheckersDefaults.PREFS_PLAYER_COLOUR_DEFAULT_VALUE);
			this.difficulty			= CheckersDefaults.PREFS_DIFFICULTY_DEFAULT_VALUE;
			this.alphaBetaPruning	= CheckersDefaults.PREFS_ALPHA_BETA_PRUNING_DEFAULT_VALUE;
		}
	}
	
	private void storePreferences()
	{
		if (!this.applet) {
			Preferences prefs = Preferences.userNodeForPackage(this.getClass());
			
			prefs.putBoolean(CheckersDefaults.PREFS_FORCE_CAPTURES_KEY, this.forceCaptures);
			prefs.putInt(CheckersDefaults.PREFS_PLAYER_COLOUR_KEY, this.playerColour.getValue());
			prefs.putInt(CheckersDefaults.PREFS_DIFFICULTY_KEY, CheckersUtils.sanitiseDifficulty(this.difficulty));
			prefs.putBoolean(CheckersDefaults.PREFS_ALPHA_BETA_PRUNING_KEY, this.alphaBetaPruning);
		}
	}
	
	public void skynetsTurn()
	{
		//CheckersMoveSequence skynetsMove = this.model.runSkynet(this.alphaBetaPruning);
		CheckersSkynetMove skynetsMove = this.model.runSkynet(this.alphaBetaPruning, this.forceCaptures);
		
		// No more successors, skynet loses, player wins
		if (skynetsMove == null) {
			this.view.playerWinGame();
			
		// Player wins
		} else if (this.model.hasPlayerWon()) {
			this.view.playerWinGame();
			
		// Skynet wins
		} else if (this.model.hasSkynetWon()) {
			this.view.renderMove(skynetsMove);
			this.view.playerLoseGame();
		
		// Skynet wins
		} else if (!this.model.hasPlayerMoreMoves(this.forceCaptures)) {
			this.view.renderMove(skynetsMove);
			this.view.playerLoseGame();
		// Normal Move
		} else {
			this.view.renderMove(skynetsMove);
			
			this.flipMovingPlayer();
			this.view.toggleViewEnabled(true);
			this.view.togglePiecesEnabled(true, this.playerColour);
			//System.out.println("SKYNETS MOVE: " + skynetsMove.getMoveSequence());
			this.model.printCurrentState();
			
		}
		
		//return skynetsMove;
	}
	
	public CheckersMoveSequence generateHint()
	{
		return this.model.generateHint(this.playerColour, this.alphaBetaPruning, this.forceCaptures);
	}
}
