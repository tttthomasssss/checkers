package com.tttthomasssss.controllers;

import java.io.Serializable;

import com.tttthomasssss.defaults.CheckersDefaults.PieceColour;
import com.tttthomasssss.defaults.CheckersUtils;
import com.tttthomasssss.models.CheckersBoardState;
import com.tttthomasssss.models.CheckersPiece;
import com.tttthomasssss.models.CheckersPosition;

public class ValidationController
implements Serializable
{
	
	private static final long serialVersionUID = -3923122635385527670L;
	private static final ValidationController INSTANCE = new ValidationController();
	
	private ValidationController()
	{
		super();
	}
	
	public static ValidationController getInstance()
	{
		return INSTANCE;
	}
	
	public boolean validPlayerMove(CheckersPosition from, CheckersPosition to, CheckersBoardState currBoardState, boolean forceCaptures)
	{
		if (!forceCaptures || (forceCaptures && CheckersUtils.isCapture(from, to))) {
			return this.validMove(from, to, currBoardState);
		} else {
			boolean validMove = this.validMove(from, to, currBoardState);
			
			return validMove && !this.hasOpenCaptures(from, currBoardState);
		}
	}
	
	public boolean validMove(CheckersPosition from, CheckersPosition to, CheckersBoardState currBoardState)
	{
		// TODO:	Due to the fact that kings can jump forwards & backwards in a single move, it screws up the current strategy
		//			Need to work out how to handle that
		
		// Differentiate between normal moves, single jumps and multi jumps
		boolean normalMove = Math.abs(from.getRow() - to.getRow()) == 1;
		boolean singleJump = Math.abs(from.getRow() - to.getRow()) == 2;
		
		
		boolean valid = false;
		
		if (!currBoardState.isEmptyAt(from) && !to.positionOutOfBounds() && currBoardState.isEmptyAt(to)) {
			
			// Differentiate colour
			int rowHelper = currBoardState.isWhiteAt(from) ? -1 : 1;
			PieceColour currColour = currBoardState.isWhiteAt(from) ? PieceColour.WHITE : PieceColour.BLACK;
			
			
			// Normal forward or backward (Kings only) move
			if (normalMove) {
				// Forward move
				if (from.getRow() + rowHelper == to.getRow() && (from.getCol() + 1 == to.getCol() || from.getCol() - 1 == to.getCol())) {
					valid = true;
				
				// Backward move (Kings only)
				} else if (currBoardState.isKingAt(from) && (from.getRow() - rowHelper == to.getRow() && (from.getCol() + 1 == to.getCol() || from.getCol() - 1 == to.getCol()))) {
					valid = true;
				}
			
			// Single forward or backward (Kings only) jump
			} else if (singleJump) {
				// Forward jump
				
				// Check if there actually are enemy pieces in the correct place
				CheckersPiece enemyPiece = currBoardState.getPieceAt(new CheckersPosition(from.getRow() + rowHelper, from.getCol() + 1));
				boolean enemyPieceFwdOneSide = enemyPiece != null && enemyPiece.isEnemyColour(currColour);
				
				enemyPiece = currBoardState.getPieceAt(new CheckersPosition(from.getRow() + rowHelper, from.getCol() - 1));
				boolean enemyPieceFwdOtherSide = enemyPiece != null && enemyPiece.isEnemyColour(currColour);
				
				// Valid forward Jump if 
				//	a) It is a diagonal forward jump to the side currCol + 2
				//	and
				//	b) There is an enemy piece diagonally forward at currCol + 1
				if (from.getRow() + (rowHelper * 2) == to.getRow() && from.getCol() + 2 == to.getCol() && enemyPieceFwdOneSide) {
					valid = true;
				}
				
				// Valid forward Jump if
				//	a) It is a diagonal forward jump to the side currCol - 2
				//	and
				//	b) There is an enemy piece diagonally forward at currCol - 1
				if (from.getRow() + (rowHelper * 2) == to.getRow() && from.getCol() - 2 == to.getCol() && enemyPieceFwdOtherSide) {
					valid = true;
				}
				
				// Backward jump (Kings only)
				if (currBoardState.isKingAt(from)) {
					enemyPiece = currBoardState.getPieceAt(new CheckersPosition(from.getRow() - rowHelper, from.getCol() + 1));
					boolean enemyPieceBwdOneSide = enemyPiece != null && enemyPiece.isEnemyColour(currColour);
				
					enemyPiece = currBoardState.getPieceAt(new CheckersPosition(from.getRow() - rowHelper, from.getCol() - 1));
					boolean enemyPieceBwdOtherSide = enemyPiece != null && enemyPiece.isEnemyColour(currColour);
				
					// Valid backward Jump if
					// 	a) currentPiece is a king
					//	and
					//	b) It is a diagonal backward jump to the side currCol + 2
					//	and
					//	c) There is an enemy piece diagonally backward at currCol + 1
					if ((from.getRow() - (rowHelper * 2) == to.getRow() && from.getCol() + 2 == to.getCol()) && enemyPieceBwdOneSide) {
						valid = true;
					}
				
					// Valid backward Jump if
					// 	a) currentPiece is a king
					//	and
					//	b) It is a diagonal backward jump to the side currCol - 2
					//	and
					//	c) There is an enemy piece diagonally backward at currCol - 1
					if ((from.getRow() - (rowHelper * 2) == to.getRow() && from.getCol() - 2 == to.getCol()) && enemyPieceBwdOtherSide) {
						valid = true;
					}
				}
			} 
		}
		
		return valid;
	}
	
	public boolean hasOpenCapturesFromPosition(CheckersPosition pos, CheckersBoardState currBoardState)
	{
		int rowHelper = currBoardState.isWhiteAt(pos) ? -1 : 1;
		PieceColour col = currBoardState.getPieceAt(pos).getColour();
		
		// Check if there actually are enemy pieces in the correct place
		CheckersPiece enemyPiece = currBoardState.getPieceAt(new CheckersPosition(pos.getRow() + rowHelper, pos.getCol() + 1));
		boolean enemyPieceFwdOneSide = enemyPiece != null && enemyPiece.isEnemyColour(col);
		
		enemyPiece = currBoardState.getPieceAt(new CheckersPosition(pos.getRow() + rowHelper, pos.getCol() - 1));
		boolean enemyPieceFwdOtherSide = enemyPiece != null && enemyPiece.isEnemyColour(col);
		
		// Check if the spaces beyond the enemy pieces are empty
		CheckersPosition empty = new CheckersPosition(pos.getRow() + (2 * rowHelper), pos.getCol() + 2);
		boolean openCaptureFwdOneSide = enemyPieceFwdOneSide && !empty.positionOutOfBounds() && currBoardState.isEmptyAt(empty);
		
		empty = new CheckersPosition(pos.getRow() + (2 * rowHelper), pos.getCol() - 2);
		boolean openCaptureFwdOtherSide = enemyPieceFwdOtherSide && !empty.positionOutOfBounds() && currBoardState.isEmptyAt(empty);
		
		if (openCaptureFwdOneSide || openCaptureFwdOtherSide) {
			return true;
		} else if (currBoardState.isKingAt(pos)) {
			// Check for King captures!
			enemyPiece = currBoardState.getPieceAt(new CheckersPosition(pos.getRow() - rowHelper, pos.getCol() + 1));
			boolean enemyPieceBwdOneSide = enemyPiece != null && enemyPiece.isEnemyColour(col);
		
			enemyPiece = currBoardState.getPieceAt(new CheckersPosition(pos.getRow() - rowHelper, pos.getCol() - 1));
			boolean enemyPieceBwdOtherSide = enemyPiece != null && enemyPiece.isEnemyColour(col);
			
			// Check if the spaces beyond the enemy pieces are empty
			empty = new CheckersPosition(pos.getRow() - (2 * rowHelper), pos.getCol() + 2);
			boolean openCaptureBwdOneSide = enemyPieceBwdOneSide && !empty.positionOutOfBounds() && currBoardState.isEmptyAt(empty);
			
			empty = new CheckersPosition(pos.getRow() - (2 * rowHelper), pos.getCol() - 2);
			boolean openCaptureBwdOtherSide = enemyPieceBwdOtherSide && !empty.positionOutOfBounds() && currBoardState.isEmptyAt(empty);
			
			return (openCaptureBwdOneSide || openCaptureBwdOtherSide);
		}
		
		return false;

	}
	
	private boolean hasOpenCaptures(CheckersPosition from, CheckersBoardState currBoardState)
	{
		PieceColour col = currBoardState.getPieceAt(from).getColour();
		
		CheckersPosition pos = null;
		
		boolean openCaptures = false;
		
		int i = 0;
		while (i < currBoardState.getRows() && !openCaptures) {
			int j = 0;
			while (j < currBoardState.getCols() && !openCaptures) {
				
				pos = new CheckersPosition(i, j);
				if (!currBoardState.isEmptyAt(pos) && currBoardState.getPieceAt(pos).getColour() == col && from != pos) {
					openCaptures = this.hasOpenCapturesFromPosition(pos, currBoardState);
				}
				
				j++;
			}
			i++;
		}
		
		//System.out.println("######## OPEN CAPTURE: " + openCaptures);
		
		return openCaptures;
	}
}
