package com.tttthomasssss.views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.IOException;
import java.io.Serializable;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.TransferHandler;

import com.tttthomasssss.controllers.ValidationController;
import com.tttthomasssss.interfaces.CheckersTileViewDelegate;
import com.tttthomasssss.models.CheckersPosition;

public class CheckersTileView extends JPanel implements DropTargetListener, Serializable {
	
	private static final long serialVersionUID = 1L;
	public static final int PREFERRED_HEIGHT = 80;
	public static final int PREFERRED_WIDTH = 80;
	
	private int posX;
	private int posY;
	
	//25/11/13
	private DropTarget dropTarget;
	
	private CheckersTileViewDelegate delegate;
	
	public CheckersTileView(int posX, int posY, Color backgroundColour)
	{
		super();
		
		super.setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));
		super.setBackground(backgroundColour);
		
		//new CheckersDropTargetListener(this);
		//25/11/13
		//super.setTransferHandler(new CheckersTransferHandler());
		
		// XXX 25/11/13
		//new DropTarget(this, DnDConstants.ACTION_MOVE, this);
		
		// 25/11/13
		this.dropTarget = new DropTarget(this, DnDConstants.ACTION_MOVE, this, true);
		
		this.posX = posX;
		this.posY = posY;
	}

	public void setDelegate(CheckersTileViewDelegate delegate)
	{
		this.delegate = delegate;
	}
	
	public CheckersTileViewDelegate getDelegate()
	{
		return this.delegate;
	}
	
	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	public void addCheckersPieceView(CheckersPieceView pieceView)
	{
		if (this.getComponentCount() == 0 && pieceView.getPieceColour() == delegate.getPlayerColour()) {
			
			CheckersPosition oldPos = pieceView.getCurrentPosition();
			CheckersPosition newPos = new CheckersPosition(this.posX, this.posY);
			
			this.delegate.movePiece(oldPos, newPos);
		}
	}
	

	@Override
	public void dragEnter(DropTargetDragEvent dtde) 
	{
		// TODO Auto-generated method stub
		
		dtde.acceptDrag(DnDConstants.ACTION_MOVE);
	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) 
	{
		// TODO Auto-generated method stub
		dtde.acceptDrag(DnDConstants.ACTION_MOVE);
	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) 
	{
		// TODO Auto-generated method stub
		dtde.acceptDrag(DnDConstants.ACTION_MOVE);
	}

	@Override
	public void dragExit(DropTargetEvent dte) 
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void drop(DropTargetDropEvent dtde) 
	{
		// XXX
		if (dtde.isDataFlavorSupported(CheckersPieceView.CHECKERS_FLAVOUR)) {
			dtde.acceptDrop(dtde.getDropAction());
			Transferable t = dtde.getTransferable();
			try {
				CheckersPieceView pieceView = (CheckersPieceView)t.getTransferData(CheckersPieceView.CHECKERS_FLAVOUR);
				
				addCheckersPieceView(pieceView);
				
				dtde.dropComplete(true);
			} catch (UnsupportedFlavorException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			dtde.rejectDrop();
		}
		
	}

}
