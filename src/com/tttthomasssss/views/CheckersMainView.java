package com.tttthomasssss.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.util.HashMap;
import java.util.Stack;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.TransferHandler;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.tttthomasssss.controllers.CheckersController;
import com.tttthomasssss.defaults.CheckersDefaults;
import com.tttthomasssss.defaults.CheckersUtils;
import com.tttthomasssss.defaults.CheckersDefaults.PieceColour;
import com.tttthomasssss.defaults.CheckersDefaults.PieceIdentity;
import com.tttthomasssss.interfaces.CheckersPieceViewDelegate;
import com.tttthomasssss.interfaces.CheckersTileViewDelegate;
import com.tttthomasssss.models.CheckersBoardState;
import com.tttthomasssss.models.CheckersMoveSequence;
import com.tttthomasssss.models.CheckersPiece;
import com.tttthomasssss.models.CheckersPosition;
import com.tttthomasssss.models.CheckersSkynetMove;

// Swing Event Queue: http://stackoverflow.com/questions/2548262/java-awt-swing-handling-the-event-dispatcher-thread
// More Drag&Drop: http://www.javaexamples.org/java/java.awt.dnd/how-to-implement-dragsourcelistener.html
// http://www.andreas-rozek.de/Java/JavaKurs/Vortrag/sld001.htm
// http://docs.oracle.com/javase/tutorial/uiswing/examples/dnd/
// https://eyeasme.com/Shayne/JAVA_DND_EXAMPLE/
// http://www.dreamincode.net/forums/topic/209966-java-drag-and-drop-tutorial-part-1-basics-of-dragging/


// http://www.javaworld.com/javaworld/jw-03-1999/jw-03-dragndrop.html?page=4
// http://www.javaworld.com/javaworld/jw-08-1999/jw-08-draganddrop.html
// http://proghammer.wordpress.com/2010/08/10/chess01-dragging-game-pieces/

public class CheckersMainView 
extends JFrame 
implements CheckersTileViewDelegate, CheckersPieceViewDelegate {

	private static final long serialVersionUID = -4385932946460794783L;

	// Controller
	private CheckersController controller;
	
	// GameBoard Setup
	private int rows;
	private int cols;
	
	// MainForm Components
	private Container	mainPain;
	private JPanel		gameBoard;
	private CheckersTileView[][] checkersTiles;
	private HashMap<CheckersPosition, CheckersPieceView> checkersPieces;
	
	// GameControls Components
	private JPanel		eastPanel;
	private JPanel		controlPanel;
	private JPanel		settingsPanel;
	private JPanel		labelPanel;
	private JLabel		lblSettings;
	private JPanel		capturesPanel;
	private JCheckBox	cbForceCaptures;
	private JPanel		playerPanel;
	private JPanel		playerLabelPanel;
	private JPanel		playerBoxPanel;
	private JLabel		lblPlayerColour;
	private JComboBox<PieceColour>	cmbPlayerColour;
	private JPanel		difficultyPanel;
	private JPanel		difficultyLabelPanel;
	private JPanel		difficultySliderPanel;
	private JLabel		lblDifficulty;
	private JSlider		slDifficulty;
	
	private JPanel		aiAndButtonPanel;
	private JPanel		aiPanel;
	private JPanel		aiLabelPanel;
	private JLabel		lblAISettings;
	private JPanel		alphaBetaPanel;
	private JCheckBox	cbAlphaBetaPruning;
	
	private JPanel		gameButtonPanel;
	private JPanel		gameButtonLabelPanel;
	private JLabel		gameButtonLabel;
	private	JPanel		newPanel;
	private JButton		btnNew;
	private JPanel		residePanel;
	private JButton		btnReside;
	private JPanel		quitPanel;
	private JButton		btnQuit;
	
	// Footer Components
	private JPanel		footerPanel;
	private JPanel		statesPanel;
	private JLabel		lblGameStatesExplored;
	private JPanel		buttonPanel;
	private JButton		btnDisplayHint;
	private JPanel		whitePiecesPanel;
	private JLabel		lblWhitePiecesLeft;
	private JPanel		blackPiecesPanel;
	private JLabel		lblBlackPiecesLeft;
	private JPanel		movingPlayerPanel;
	private JLabel		lblMovingPlayer;
	
	// MenuBar Components
    private JMenuBar	menuBar;
    private JMenu       gameMenu;
    private JMenuItem   newItem;
    private JMenuItem	resideItem;
    private JMenuItem   quitItem;
    private JMenu       helpMenu;
    private JMenuItem	hintItem;
    private JMenuItem	tutorialItem;
    private JMenuItem   aboutItem;
	
    // Tutorial View
    private CheckersTutorialView tutorialView;
    
    // MoveSequence Stuff
    private Stack<CheckersPosition> hintPositions;
    private Timer					hintTimer;
    private CheckersSkynetMove		currentSkynetMove;
    private Timer					timer;
    private boolean					playerMoveFinished;
    
	public CheckersMainView(CheckersController controller)
	{
		this(controller, CheckersDefaults.DFLT_NR_OF_ROWS, CheckersDefaults.DFLT_NR_OF_COLS);
	}
	
	public CheckersMainView(CheckersController controller, int rows, int cols)
	{
		super("Jeckers");
		
		this.controller = controller;
		this.rows = rows;
		this.cols = cols;
		this.playerMoveFinished = false;
		
		this.checkersTiles	= new CheckersTileView[this.rows][this.cols];
		this.checkersPieces	= new HashMap<CheckersPosition, CheckersPieceView>();
		
		super.setTransferHandler(new TransferHandler("icon"));
		
		this.initGUI();
		
		this.togglePiecesEnabled(false, null);
	}
	
	public CheckersController getController()
	{
		return this.controller;
	}
	
	public void setController(CheckersController controller)
	{
		this.controller = controller;
	}
	
	public boolean hasPlayerMoveFinished()
	{
		return this.playerMoveFinished;
	}
	
	public void setPlayerMoveFinished(boolean fin)
	{
		this.playerMoveFinished = fin;
	}
	
	public void displayGameState(CheckersBoardState state)
	{
		for (int i = 0; i < state.getRows(); i++) {
			for (int j = 0; j < state.getCols(); j++) {
				CheckersPosition currPos = new CheckersPosition(i, j);
				if (!state.isEmptyAt(currPos)) {
					this.renderPieceAt(currPos, state.getPieceAt(currPos));
				}
			}
		}
	}
	
	public void updateExploreCount(int exploreCount)
	{
		this.lblGameStatesExplored.setText("States explored: " + exploreCount);
	}
	
	public void resetLabels()
	{
		this.lblGameStatesExplored.setText("States explored: 0");
		this.lblWhitePiecesLeft.setText("White pieces left: " + CheckersDefaults.DFLT_NR_OF_PIECES);
		this.lblBlackPiecesLeft.setText("Black pieces left: " + CheckersDefaults.DFLT_NR_OF_PIECES);
		this.lblMovingPlayer.setText("Moving Player: None");
		this.btnNew.setText("Start Game");
		this.newItem.setText("Start Game");
		this.btnNew.setEnabled(true);
		this.newItem.setEnabled(true);
		this.updateShowHintControls(this.controller.getDifficulty());
	}
	
	public void updateMovingPlayerLabel(PieceColour turn)
	{
		this.lblMovingPlayer.setText("Moving Player: " + turn);
	}
	
	public void renderMove(CheckersSkynetMove move)
	{
		this.currentSkynetMove = move;
		
		this.timer = new Timer(CheckersDefaults.DFLT_TIMER_DELAY, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (currentSkynetMove.getMoveSequence().hasMoreMoves()) {
					
					// Move Piece from current position...
					CheckersPosition currPos = currentSkynetMove.getMoveSequence().popFirst();
					CheckersPieceView pieceView = checkersPieces.remove(currPos);
					checkersTiles[currPos.getRow()][currPos.getCol()].removeAll();
					
					// ...to new position
					CheckersPosition newPos = currentSkynetMove.getMoveSequence().peekFirst();
					pieceView.setCurrentPosition(newPos);
					checkersPieces.put(newPos, pieceView);
					checkersTiles[newPos.getRow()][newPos.getCol()].add(pieceView);
					
					// Was move a capture?
					if (currentSkynetMove.isCapture()) {
						capturePieceAt(CheckersUtils.getCapturedPiecePosition(currPos, newPos));
					}
					
					// Repaint and validate the stuff
					repaint();
					revalidate();
				} else {
					// Was move a king promotion?
					if (currentSkynetMove.canPromoteToKing()) {
						promotePieceToKingAtPos(currentSkynetMove.getMoveSequence().peekLastPosition());
					}
					timer.stop();
					timer = null;
					updatePieceCount(currentSkynetMove.getWhitePiecesLeft(), currentSkynetMove.getBlackPiecesLeft());
					currentSkynetMove = null;
				}
			}
			
		});
		this.timer.start();
	}
	
	private void initGUI()
	{
		// Setup MenuBar
		this.initMenuBar();
		
		// Main Pain Business
		this.mainPain = this.getContentPane();
		this.mainPain.setLayout(new BorderLayout());
		this.initGameBoard();
		
		// Setup the Game Controls
		this.initGameControls();
		
		// Setup the Footer
		this.initFooter();
		
		// Initialise the Game Pieces
		this.initPieces();
		//this.initPiecesWithNullPointerPosition();
		//this.initPiecesMultiJumpBoard();
		//this.initPiecesMultiJumpKingBoard();
		//this.initKingPromotionBoard();
		
		// Reset Labels and other Control states
		this.resetLabels();
		this.updateShowHintControls(controller.getDifficulty());
		
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.pack();
		
		this.setLocationRelativeTo(null);
		//this.setVisible(true);
	}
	
	private void initMenuBar()
	{
		// The Menubar
		this.menuBar = new JMenuBar();
		this.setJMenuBar(this.menuBar);
		
		// 	--> Game
		this.gameMenu = new JMenu("Game");
		
		//		--> New Game
		this.newItem = new JMenuItem("New Game");
		this.newItem.setAccelerator(KeyStroke.getKeyStroke('N', InputEvent.CTRL_DOWN_MASK));
		this.newItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				controller.startGame();
				activateGame();
			}
			
		});
		this.gameMenu.add(this.newItem);
		
		//		--> Reside current Game
		this.resideItem = new JMenuItem("Resign current Game");
		this.resideItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// Tell the controller
				resignCurrentGame();
			}
		});
		this.gameMenu.add(this.resideItem);
		this.gameMenu.addSeparator();
		
		//		--> Quit Checkers
		this.quitItem = new JMenuItem("Quit Checkers");
		this.quitItem.setAccelerator(KeyStroke.getKeyStroke('Q', InputEvent.CTRL_DOWN_MASK));
		this.quitItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// Tell the controller
				controller.quit(0);
			}
		});
		this.gameMenu.add(this.quitItem);
		
		this.menuBar.add(this.gameMenu);
		
		// --> ?
		this.helpMenu = new JMenu("?");
		
		//		--> Show Hint
		this.hintItem = new JMenuItem("Show Hint");
		this.hintItem.setAccelerator(KeyStroke.getKeyStroke('H', InputEvent.CTRL_DOWN_MASK));
		this.hintItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				displayHint();
			}
		});
		this.helpMenu.add(this.hintItem);
		
		//		--> Show Tutorial
		this.tutorialItem = new JMenuItem("Show Tutorial");
		this.tutorialItem.setAccelerator(KeyStroke.getKeyStroke('T', InputEvent.CTRL_DOWN_MASK));
		this.tutorialItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				displayTutorialView();
			}
		});
		this.helpMenu.add(this.tutorialItem);
		this.helpMenu.addSeparator();
		
		//		--> About
		this.aboutItem = new JMenuItem("About");
		this.aboutItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				displayAboutWindow();
			}
		});
		this.helpMenu.add(this.aboutItem);
		
		this.menuBar.add(this.helpMenu);
	}
	
	private void initGameBoard()
	{
		this.gameBoard = new JPanel();
		
		this.gameBoard.setLayout(new GridLayout(this.rows, this.cols));
		
		boolean lightColourToggle = true;
		
		for (int i = 0; i < this.rows; i++) {
			for (int j = 0; j < this.cols; j++) {
				
				Color c = lightColourToggle ? CheckersUtils.getLightTileColour() : CheckersUtils.getDarkTileColour();
				lightColourToggle = !lightColourToggle;
				this.checkersTiles[i][j] = new CheckersTileView(i, j, c);
				this.checkersTiles[i][j].setLayout(new FlowLayout());
				this.checkersTiles[i][j].setDelegate(this);
				
				this.gameBoard.add(this.checkersTiles[i][j]);
			}
			lightColourToggle = !lightColourToggle;
		}
		
		this.mainPain.add(this.gameBoard, BorderLayout.CENTER);
	}
	
	private void initGameControls()
	{
		this.eastPanel = new JPanel(new FlowLayout());
		
		this.controlPanel = new JPanel(new GridLayout(2, 1));
		
		this.settingsPanel = new JPanel(new GridLayout(4, 1));
		
		// Game Settings Label
		this.labelPanel = new JPanel(new FlowLayout());
		this.lblSettings = new JLabel("Game Settings");
		this.lblSettings.setFont(this.lblSettings.getFont().deriveFont(20.f));
		this.labelPanel.add(this.lblSettings);
		this.settingsPanel.add(this.labelPanel);
		
		// Force Captures Checkbox
		this.capturesPanel = new JPanel(new FlowLayout());
		this.cbForceCaptures = new JCheckBox("Force Captures", this.controller.isForceCaptures());
		this.cbForceCaptures.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				JCheckBox box = (JCheckBox)e.getSource();
				controller.setForceCaptures(box.isSelected());
			}
			
		});
		this.capturesPanel.add(this.cbForceCaptures);
		this.settingsPanel.add(this.capturesPanel);
		
		// Player Colour Label & Combobox
		this.playerPanel = new JPanel(new GridLayout(2, 1));
		
		this.playerLabelPanel = new JPanel(new FlowLayout());
		this.lblPlayerColour = new JLabel("Player Colour");
		this.playerLabelPanel.add(this.lblPlayerColour);
		this.playerPanel.add(this.playerLabelPanel);
		
		this.playerBoxPanel = new JPanel(new FlowLayout());
		this.cmbPlayerColour = new JComboBox<PieceColour>(CheckersUtils.getGameColours());
		this.cmbPlayerColour.setPreferredSize(new Dimension(115, 24));
		this.cmbPlayerColour.setSelectedItem(this.controller.getPlayerColour());
		this.cmbPlayerColour.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				JComboBox<PieceColour> cmb = (JComboBox<PieceColour>)e.getSource();
				PieceColour selectedColour = (PieceColour)cmb.getSelectedItem();
				controller.setPlayerColour(selectedColour);
				controller.initGame();
				togglePiecesEnabled(true, controller.getPlayerColour());
			}
		});
		this.playerBoxPanel.add(this.cmbPlayerColour);
		this.playerPanel.add(this.playerBoxPanel);
		
		this.settingsPanel.add(this.playerPanel);
		
		// Game Difficulty Label & Slider
		this.difficultyPanel = new JPanel(new GridLayout(2, 1));
		
		this.difficultyLabelPanel = new JPanel(new FlowLayout());
		this.lblDifficulty = new JLabel("Difficulty");
		this.difficultyLabelPanel.add(this.lblDifficulty);
		this.difficultyPanel.add(this.difficultyLabelPanel);
		
		this.difficultySliderPanel = new JPanel(new FlowLayout());
		this.slDifficulty = new JSlider(CheckersDefaults.DIFFICULTY_MIN_VALUE, CheckersDefaults.DIFFICULTY_MAX_VALUE, CheckersDefaults.PREFS_DIFFICULTY_DEFAULT_VALUE);
		this.slDifficulty.setValue(this.controller.getDifficulty());
		this.slDifficulty.setMajorTickSpacing(1);
		this.slDifficulty.setPaintTicks(true);
		//this.slDifficulty.setPaintLabels(true);
		this.slDifficulty.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e)
			{
				JSlider sl = (JSlider)e.getSource();
				updateShowHintControls(sl.getValue());
				controller.setDifficulty(sl.getValue());
			}
		});
		this.difficultySliderPanel.add(this.slDifficulty);
		this.difficultyPanel.add(this.difficultySliderPanel);
		
		this.settingsPanel.add(this.difficultyPanel);
		
		// AI Settings
		this.aiAndButtonPanel = new JPanel(new GridLayout(2, 1));
		
		this.aiPanel = new JPanel(new GridLayout(2, 1));
		
		this.aiLabelPanel = new JPanel(new FlowLayout());
		this.lblAISettings = new JLabel("AI Settings");
		this.lblAISettings.setFont(this.lblAISettings.getFont().deriveFont(20.f));
		this.aiLabelPanel.add(this.lblAISettings);
		this.aiPanel.add(this.aiLabelPanel);
		this.aiAndButtonPanel.add(this.aiPanel);
		
		this.alphaBetaPanel = new JPanel(new FlowLayout());
		this.cbAlphaBetaPruning = new JCheckBox("Alpha-Beta Pruning", this.controller.isAlphaBetaPruning());
		this.cbAlphaBetaPruning.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				JCheckBox box = (JCheckBox)e.getSource();
				lblDifficulty.setText("Difficulty (Depth=" + CheckersUtils.getMaxSearchDepthForDifficultyAndPruningOption(slDifficulty.getValue(), box.isSelected()) + ")");
				controller.setAlphaBetaPruning(box.isSelected());
			}
			
		});
		this.alphaBetaPanel.add(this.cbAlphaBetaPruning);
		this.aiPanel.add(this.alphaBetaPanel);
		this.aiAndButtonPanel.add(this.aiPanel);
		
		// Buttons
		this.gameButtonPanel = new JPanel(new GridLayout(4, 1));
		
		this.gameButtonLabelPanel = new JPanel(new FlowLayout());
		this.gameButtonLabel = new JLabel("Game");
		this.gameButtonLabel.setFont(this.gameButtonLabel.getFont().deriveFont(20.f));
		this.gameButtonLabelPanel.add(this.gameButtonLabel);
		this.gameButtonPanel.add(this.gameButtonLabelPanel);
		
		this.newPanel = new JPanel(new FlowLayout());
		this.btnNew = new JButton("Start Game");
		this.btnNew.setPreferredSize(new Dimension(115, 24));
		this.btnNew.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				controller.startGame();
				activateGame();
			}
		});
		this.newPanel.add(this.btnNew);
		this.gameButtonPanel.add(this.newPanel);
		
		this.residePanel = new JPanel(new FlowLayout());
		this.btnReside = new JButton("Resign Game");
		this.btnReside.setPreferredSize(new Dimension(115, 24));
		this.btnReside.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				resignCurrentGame();
			}
		});
		this.residePanel.add(this.btnReside);
		this.gameButtonPanel.add(this.residePanel);
		
		this.quitPanel = new JPanel(new FlowLayout());
		this.btnQuit = new JButton("Quit Game");
		this.btnQuit.setPreferredSize(new Dimension(115, 24));
		this.btnQuit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				controller.quit(0);
			}
		});
		this.quitPanel.add(this.btnQuit);
		this.gameButtonPanel.add(this.quitPanel);
		
		this.aiAndButtonPanel.add(this.gameButtonPanel);
		
		this.controlPanel.add(this.settingsPanel);
		this.controlPanel.add(this.aiAndButtonPanel);
		
		this.eastPanel.add(this.controlPanel);
		
		this.eastPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		this.mainPain.add(this.eastPanel, BorderLayout.EAST);
	}
	
	private void initFooter()
	{
		this.footerPanel = new JPanel(new GridLayout(1, 5));
		
		this.statesPanel = new JPanel(new FlowLayout());
		this.lblGameStatesExplored = new JLabel("States explored: 0");
		this.statesPanel.add(this.lblGameStatesExplored);
		this.footerPanel.add(this.statesPanel);
		
		this.buttonPanel = new JPanel(new FlowLayout());
		this.btnDisplayHint = new JButton("Hint");
		this.btnDisplayHint.setPreferredSize(new Dimension(64, 16));
		this.btnDisplayHint.setBackground(new Color(85, 107, 47));
		this.btnDisplayHint.setForeground(Color.WHITE);
		this.btnDisplayHint.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				displayHint();
			}
			
		});
		this.buttonPanel.add(this.btnDisplayHint);
		this.footerPanel.add(this.buttonPanel);
		
		this.movingPlayerPanel = new JPanel(new FlowLayout());
		this.lblMovingPlayer = new JLabel("Moving Player: None");
		this.movingPlayerPanel.add(this.lblMovingPlayer);
		this.footerPanel.add(this.movingPlayerPanel);
		
		this.whitePiecesPanel = new JPanel(new FlowLayout());
		this.lblWhitePiecesLeft = new JLabel("White pieces left: " + CheckersDefaults.DFLT_NR_OF_PIECES);
		this.whitePiecesPanel.add(this.lblWhitePiecesLeft);
		this.footerPanel.add(this.whitePiecesPanel);
		
		this.blackPiecesPanel = new JPanel(new FlowLayout());
		this.lblBlackPiecesLeft = new JLabel("Black pieces left: " + CheckersDefaults.DFLT_NR_OF_PIECES);
		this.blackPiecesPanel.add(this.lblBlackPiecesLeft);
		this.footerPanel.add(this.blackPiecesPanel);
		
		this.footerPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		this.mainPain.add(this.footerPanel, BorderLayout.SOUTH);
	}
	
	private void initPieces()
	{
		int start = 1;
		
		CheckersPieceView piece = null;
		CheckersPosition pos = null;
		
		for (int i = 0; i < 3; i++) {
			start = Math.abs((i % 2) - 1);
			for (int j = start; j < this.cols; j += 2) {
				pos = new CheckersPosition(i, j);
				piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
				piece.setDelegate(this);
				this.checkersPieces.put(new CheckersPosition(i, j), piece);
			}
		}
		
		// Setup Human controlled pieces
		for (int i = this.rows - 1; i > this.rows - 4; i--) {
			start = Math.abs((i % 2) - 1);
			for (int j = start; j < this.cols; j+= 2) {
				pos = new CheckersPosition(i, j);
				piece = new CheckersPieceView(PieceColour.WHITE, PieceIdentity.NORMAL, pos);
				piece.setDelegate(this);
				this.checkersPieces.put(pos, piece);
			}
		}
	}
	
	private void displayAboutWindow()
	{
		JOptionPane.showMessageDialog(this, "Jeckers (=JCheckers -> Jeckers, I know, the pun is rather weak...)\n\nlovingly developed and designed by Thomas Kober (19214) (c)\n\nCheckers Tutorial texts shamelessly copied from: http://www.jimloy.com/checkers/rules2.htm", "About Jeckers", JOptionPane.INFORMATION_MESSAGE);
	}
	
	private void displayTutorialView()
	{
		// Lazy Init the tutorial view
		if (this.tutorialView == null) {
			this.tutorialView = new CheckersTutorialView(this);
		}
		
		this.tutorialView.showView();
	}
	
	private void renderPieceAt(CheckersPosition pos, CheckersPiece piece)
	{
		this.checkersTiles[pos.getRow()][pos.getCol()].add(this.checkersPieces.get(pos));
		this.gameBoard.repaint();
		this.gameBoard.revalidate();
	}

	private void updateShowHintControls(int difficulty)
	{
		this.lblDifficulty.setText("Difficulty (Depth=" + CheckersUtils.getMaxSearchDepthForDifficultyAndPruningOption(difficulty, controller.isAlphaBetaPruning()) + ")");
		
		if (difficulty < CheckersDefaults.DIFFICULTY_MAX_VALUE) {
			this.btnDisplayHint.setEnabled(true);
			this.btnDisplayHint.setBackground(new Color(85, 107, 47));
			this.btnDisplayHint.setForeground(Color.WHITE);
			this.hintItem.setEnabled(true);
		} else {
			this.btnDisplayHint.setEnabled(false);
			this.btnDisplayHint.setBackground(new Color(153, 0, 18));
			this.hintItem.setEnabled(false);
		}
	}
	
	private void toggleGameControlsEnabled(boolean enabled)
	{
		this.cbForceCaptures.setEnabled(enabled);
		this.cmbPlayerColour.setEnabled(enabled);
		this.slDifficulty.setEnabled(enabled);
		
		this.btnDisplayHint.setEnabled(!enabled);
		this.hintItem.setEnabled(!enabled);
	}
	
	private void checkActiveGame()
	{
		if (!this.controller.isActiveGame()) {
			this.controller.activateGame();
			
			//this.toggleGameControlsEnabled(false);
			this.activateGame();
		}
	}
	
	public void activateGame()
	{
		this.toggleGameControlsEnabled(false);
		this.btnNew.setEnabled(false);
		this.newItem.setEnabled(false);
		this.updateMovingPlayerLabel(this.controller.getMovingPlayer());
		if (this.controller.getMovingPlayer() == this.controller.getPlayerColour()) {
			this.togglePiecesEnabled(true, this.controller.getPlayerColour());
		}
	}
	
	public void toggleViewEnabled(boolean enabled)
	{
		this.setEnabled(enabled);
	}
	
	public void togglePiecesEnabled(boolean enabled, PieceColour colour)
	{
		CheckersPieceView pieceView = null;
		
		for (CheckersPosition pos : this.checkersPieces.keySet()) {
			pieceView = this.checkersPieces.get(pos);
			if (colour == null || pieceView.getPieceColour() == colour) {
				pieceView.setEnabled(enabled);
			}
		}
	}
	
	@Override
	public void validateMove(CheckersPosition from, CheckersPosition to) {
		// TODO Auto-generated method stub
		System.out.println("Probably dont need that...");
	}

	@Override
	public PieceColour getPlayerColour()
	{
		return controller.getPlayerColour();
	}
	
	@Override
	public void movePiece(CheckersPosition from, CheckersPosition to) 
	{
		if (this.controller.movePiece(from, to)) {
			
			this.checkActiveGame();
			
			// Update the View if its a valid move
			this.checkersTiles[from.getRow()][from.getCol()].removeAll();
		
			CheckersPieceView pieceView = this.checkersPieces.remove(from);
			pieceView.setCurrentPosition(to);
			this.checkersPieces.put(to, pieceView);
		
			this.checkersTiles[to.getRow()][to.getCol()].add(pieceView);
			
			this.gameBoard.repaint();
			this.gameBoard.revalidate();
		
			this.controller.getModel().printCurrentState();
			
			// Player has more moves if the current move was a capture AND there are more open captures on the board
			boolean hasMoreMoves = controller.hasPlayerOpenCapturesFromPosition(to) && CheckersUtils.isCapture(from, to);
			
			this.playerMoveFinished = !hasMoreMoves;
		} else {
			this.playerMoveFinished = false;
		}
	}
	
	@Override
	public void moveFinished()
	{
		if (this.playerMoveFinished) {
			this.controller.flipMovingPlayer();
			this.toggleViewEnabled(false);
			this.controller.skynetsTurn();
		}
	}
	
	public void capturePieceAt(CheckersPosition enemyPos)
	{
		// Remove Piece from View
		this.checkersPieces.remove(enemyPos);
		this.checkersTiles[enemyPos.getRow()][enemyPos.getCol()].removeAll();
		
		this.gameBoard.repaint();
		this.gameBoard.revalidate();
	}
	
	public void promotePieceToKingForMove(CheckersPosition oldPos, CheckersPosition newPos)
	{
		// Promote Piece to King
		CheckersPieceView pieceView = this.checkersPieces.get(oldPos);
		pieceView.promoteToKing();
	}

	public void updatePieceCount(int whitePieces, int blackPieces)
	{
		this.lblWhitePiecesLeft.setText("White pieces left: " + whitePieces);
		this.lblBlackPiecesLeft.setText("Black pieces left: " + blackPieces);
	}
	
	public void promotePieceToKingAtPos(CheckersPosition pos)
	{
		CheckersPieceView pieceView = this.checkersPieces.get(pos);
		pieceView.promoteToKing();
	}
	
	public void playerLoseGame()
	{
		Object[] options = {"Yes", "No"};
		int option = JOptionPane.showOptionDialog(this, "Teh Noes! You have lost the game!\n\nWant another round?", "You FAIL!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
		
		if (option == 0) {
			this.resetTiles();
			this.initPieces();
			this.resetLabels();
			this.toggleViewEnabled(true);
			this.toggleGameControlsEnabled(true);
			this.controller.resetGame();
		} else {
			this.controller.quit(0);
		}
	}
	
	public void playerWinGame()
	{
		Object[] options = {"Yes", "No"};
		int option = JOptionPane.showOptionDialog(this, "Hooray, you've won the game!\n\nWant another round?", "You WIN!", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
		
		if (option == 0) {
			this.resetTiles();
			this.initPieces();
			this.resetLabels();
			this.toggleViewEnabled(true);
			this.toggleGameControlsEnabled(true);
			this.controller.resetGame();
		} else {
			this.controller.quit(0);
		}
	}
	
	private void resignCurrentGame()
	{
		Object[] options = {"Yes", "No", "Cancel"};
		int option = JOptionPane.showOptionDialog(this, "Are you sure you want to resign the current game?", "Resign current game", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
	
		// option = 0 => Yes, I want to resign
		if (option == 0) {
			this.resetTiles();
			this.initPieces();
			this.resetLabels();
			this.toggleGameControlsEnabled(true);
			this.controller.resetGame();
		}
	}
	
	private void displayHint()
	{
		CheckersMoveSequence hintSeq = this.controller.generateHint();
		CheckersPosition pos = hintSeq.peekFirst();
		
		this.hintPositions = new Stack<CheckersPosition>();
		
		// Display the Hint Boxes
		while (!hintSeq.isEmpty()) {
			pos = hintSeq.popFirst();
			
			this.checkersTiles[pos.getRow()][pos.getCol()].setBorder(BorderFactory.createLineBorder(Color.GREEN, 2));
			
			this.hintPositions.add(pos);
		}
		
		// Remove the Hint Boxes again
		this.hintTimer = new Timer(2000, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				CheckersPosition pos = null;
				
				while (!hintPositions.isEmpty()) {
					pos = hintPositions.pop();
					checkersTiles[pos.getRow()][pos.getCol()].setBorder(BorderFactory.createEmptyBorder());
				}
				hintTimer.stop();
				hintTimer = null;
				hintPositions = null;
			}	
		});
		this.hintTimer.start();
	}
	
	private void resetTiles()
	{
		for (int i = 0; i < this.rows; i++) {
			for (int j = 0; j < this.cols; j++) {
				this.checkersTiles[i][j].removeAll();
			}
		}
	}
	
	private void initPiecesMultiJumpBoard()
	{
		// Setup black pieces
		CheckersPieceView piece = null;
		CheckersPosition pos = null;
		
		// Setup Black Pieces
		pos = new CheckersPosition(2, 1);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(2, 3);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(2, 5);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(4, 3);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(6, 3);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(6, 1);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		// Setup white piece
		pos = new CheckersPosition(7, 2);
		piece = new CheckersPieceView(PieceColour.WHITE, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
	}
	
	private void initKingPromotionBoard()
	{
		CheckersPieceView piece = null;
		CheckersPosition pos = null;
		
		// Setup black pieces
		pos = new CheckersPosition(6, 1);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		// Setup white pieces
		pos = new CheckersPosition(1, 0);
		piece = new CheckersPieceView(PieceColour.WHITE, PieceIdentity.NORMAL, pos);
		this.checkersPieces.put(pos, piece);
	}
	
	private void initPiecesMultiJumpKingBoard()
	{
		CheckersPieceView piece = null;
		CheckersPosition pos = null;
		
		// Setup Black Pieces
		pos = new CheckersPosition(2, 1);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(2, 3);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(2, 5);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(4, 1);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(4, 3);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(4, 5);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(6, 1);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(6, 3);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(6, 5);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		// Setup white pieces
		pos = new CheckersPosition(7, 2);
		piece = new CheckersPieceView(PieceColour.WHITE, PieceIdentity.KING, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
	}
	
	private void initPiecesWithNullPointerPosition()
	{
		CheckersPieceView piece = null;
		CheckersPosition pos = null;
		
		// Setup Black Pieces
		pos = new CheckersPosition(0, 1);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(5, 6);
		piece = new CheckersPieceView(PieceColour.BLACK, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		// Setup White Pieces
		pos = new CheckersPosition(3, 0);
		piece = new CheckersPieceView(PieceColour.WHITE, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
		
		pos = new CheckersPosition(3, 4);
		piece = new CheckersPieceView(PieceColour.WHITE, PieceIdentity.NORMAL, pos);
		piece.setDelegate(this);
		this.checkersPieces.put(pos, piece);
	}
}
