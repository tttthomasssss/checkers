package com.tttthomasssss.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.tttthomasssss.defaults.CheckersImageCollection;;

public class CheckersTutorialView 
extends JDialog
{
	private static final long serialVersionUID = -6105551077259295816L;
	private Container mainPain;
	
	private JPanel mainView;
	private JPanel imageView;
	private JPanel textView;
	private JLabel imageLabel;
	private JTextArea textArea;
	private JPanel buttonPanel;
	private JButton btnNext;
	private JButton btnPrevious;
	private JButton btnClose;
	private JPanel headerPanel;
	private JLabel headerLabel;
	
	private ArrayList<String> tutorialTexts;
	private ArrayList<ImageIcon> tutorialImages;
	private ArrayList<String> headerTexts;
	private int currIndex;
	
	public CheckersTutorialView(JFrame owner)
	{
		super(owner, "Checkers Tutorial", true);
		
		this.currIndex = 0;
		this.tutorialImages = new ArrayList<ImageIcon>();
		this.tutorialTexts = new ArrayList<String>();
		this.headerTexts = new ArrayList<String>();
		
		this.loadImagesAndTexts();
		
		this.initGUI();
	}
	
	public void showView()
	{
		this.currIndex = 0;
		this.setVisible(true);
	}
	
	private void initGUI()
	{
		this.mainPain = this.getContentPane();
		this.mainPain.setLayout(new BorderLayout());
		
		this.mainView = new JPanel(new GridLayout(2, 1));
		
		// Init Text
		this.textView = new JPanel(new FlowLayout());
		String currText = this.tutorialTexts.get(this.currIndex);
				
		this.textArea = new JTextArea(currText);
		this.textArea.setEditable(false);
		this.textArea.setBackground(new Color(238, 238, 238));
		this.textView.add(this.textArea);
				
		this.mainView.add(this.textView);
		
		// Init Image
		this.imageView = new JPanel(new FlowLayout());
		ImageIcon currImg = this.tutorialImages.get(this.currIndex);
		
		this.imageLabel = (currImg != null) ? new JLabel("", currImg, JLabel.CENTER) : new JLabel("", JLabel.CENTER);
		this.imageView.add(this.imageLabel);
		
		this.mainView.add(this.imageView);
		
		this.mainPain.add(this.mainView, BorderLayout.CENTER);
		
		// Init Headers
		String currHeader = this.headerTexts.get(this.currIndex);
		this.headerPanel = new JPanel(new FlowLayout());
		this.headerLabel = new JLabel(currHeader, JLabel.CENTER);
		this.headerLabel.setFont(this.headerLabel.getFont().deriveFont(16.f));
		this.headerPanel.add(this.headerLabel);
		this.mainPain.add(headerPanel, BorderLayout.NORTH);
		
		
		// Init Buttons
		this.btnNext = new JButton("Next");
		this.btnNext.setPreferredSize(new Dimension(90, 22));
		this.btnNext.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				currIndex = Math.min(tutorialTexts.size() - 1, (currIndex + 1));
				updateMainView();
			}
			
		});
		
		this.btnPrevious = new JButton("Previous");
		this.btnPrevious.setPreferredSize(new Dimension(90, 22));
		this.btnPrevious.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				currIndex = Math.max(0, (currIndex - 1));
				updateMainView();
			}
		});
		
		this.btnClose = new JButton("Close");
		this.btnClose.setPreferredSize(new Dimension(90, 22));
		this.btnClose.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				setVisible(false);
			}
		});
		
		this.buttonPanel = new JPanel(new FlowLayout());
		this.buttonPanel.add(this.btnPrevious);
		this.buttonPanel.add(this.btnNext);
		this.buttonPanel.add(this.btnClose);
		
		this.mainPain.add(this.buttonPanel, BorderLayout.SOUTH);
		this.setPreferredSize(new Dimension(640, 640));
		this.pack();
		
		this.setLocationRelativeTo(this.getOwner());
	}
	
	private void updateMainView()
	{
		ImageIcon currImage = this.tutorialImages.get(this.currIndex);
		this.imageLabel.setIcon(currImage);
		
		String currText = this.tutorialTexts.get(this.currIndex);
		this.textArea.setText(currText);
		
		String currHeader = this.headerTexts.get(this.currIndex);
		this.headerLabel.setText(currHeader);
		
		if (this.currIndex >= this.tutorialTexts.size() - 1) {
			this.btnNext.setEnabled(false);
		} else {
			this.btnNext.setEnabled(true);
		}
		
		if (this.currIndex <= 0) {
			this.btnPrevious.setEnabled(false);
		} else {
			this.btnPrevious.setEnabled(true);
		}
		
		this.repaint();
		this.revalidate();
	}
	
	private void loadImagesAndTexts()
	{
		// Page 1, General
		String header1 = "General Information (1 / 8)";
		this.headerTexts.add(header1);
		
		String page1 = "Checkers is a board game played between two players, who alternate moves.\n" +
				"The player who cannot move, because he has no pieces, or because all of his\n" +
				"pieces are blocked, loses the game. Players can resign or agree to draws.\n\n" +
				"Black moves first. The players take turns moving. You can make only one move per turn.\n" +
				"You must move. If you cannot move, you lose.";
		
		
		this.tutorialTexts.add(page1);
		
		this.tutorialImages.add(null);
		
		// Page 2, The Board
		String header2 = "The Board (2 / 8)";
		this.headerTexts.add(header2);
		
		String page2 = "The board is square, with sixty-four smaller squares, arranged in an 8x8 grid.\n" +
				"The smaller squares are alternately light and dark colored (dark brown and light brown in this game),\n" +
				"in the famous \"checker-board\" pattern. The game of checkers is played on the dark squares.\n" +
				"Each player has a dark square on his far left and a light square on his far right. \n" +
				"The double-corner is the distinctive pair of dark squares in the near right corner.";
		this.tutorialTexts.add(page2);
		
		ImageIcon img2 = CheckersImageCollection.getInstance().getCheckersBoardIcon();
		this.tutorialImages.add(img2);
		
		// Page 3, The Pieces
		String header3 = "The Pieces (3 / 8)";
		this.headerTexts.add(header3);
		
		String page3 = "The pieces are Black and White, and are called Black and White in most books.\n" +
				"In some modern publications, they are called Red and White. Sets bought in stores may be\n" +
				"other colors. Black and Red pieces are still called Black (or Red) and White,\n" +
				"so that you can read the books. The pieces are of circular shape \n" +
				"(or cylindrical in the real world), much wider than they are tall . Tournament pieces \n" +
				"are smooth, and have no designs (crowns or concentric circles) on them. \n" +
				"The pieces are placed on the dark squares of the board.";
		this.tutorialTexts.add(page3);
		
		ImageIcon img3 = CheckersImageCollection.getInstance().getCheckersPiecesIcon();
		this.tutorialImages.add(img3);
		
		// Page 4, The Starting Position
		String header4 = "The Starting Board (4 / 8)";
		this.headerTexts.add(header4);
		
		String page4 = "The starting position is with each player having twelve pieces,\n" +
				"on the twelve dark squares closest to his edge of the board. Notice that in checker diagrams,\n" +
				"the pieces are placed on the dark colored squares.";
		this.tutorialTexts.add(page4);
		
		ImageIcon img4 = CheckersImageCollection.getInstance().getCheckersBoardStartingPositionIcon();
		this.tutorialImages.add(img4);
		
		// Page 5, Moving
		String header5 = "Moving Pieces (5 / 8)";
		this.headerTexts.add(header5);
		
		String page5 = "A piece which is not a king can move one square, diagonally, forward,\n" +
				"as in the diagram below. A king can move one square diagonally, forward or backward.\n" +
				"A piece (piece or king) can only move to a vacant square. A move can also consist of\n" +
				"one or more jumps (next page).";
		this.tutorialTexts.add(page5);
		
		ImageIcon img5 = CheckersImageCollection.getInstance().getCheckersSingleMoveIcon();
		this.tutorialImages.add(img5);
		
		// Page 6, Capturing
		String header6 = "Capturing Pieces - Single Captures (6 / 8)";
		this.headerTexts.add(header6);
		
		String page6 = "Jumping: You capture an opponent's piece (piece or king) by jumping over it,\n" +
				"diagonally, to the adjacent vacant square beyond it. The three squares must be lined up\n" +
				"(diagonally adjacent) as in the diagram below: your jumping piece (piece or king), \n" +
				"opponent's piece (piece or king), empty square. A king can jump diagonally, forward or backward.\n" +
				"A piece which is not a king, can only jump diagonally forward.";
		this.tutorialTexts.add(page6);
		
		ImageIcon img6 = CheckersImageCollection.getInstance().getCheckersCaptureIcon();
		this.tutorialImages.add(img6);
		
		// Page 7, Multiple Captures
		String header7 = "Capturing Pieces - Multi Captures (7 / 8)";
		this.headerTexts.add(header7);
		
		String page7 = "You can make a multiple jump (see the diagram below), with one piece only,\n" +
				"by jumping to empty square to empty square. In a multiple jump, the jumping piece or king\n" +
				"can change directions, jumping first in one direction and then in another direction.\n" +
				"You can only jump one piece with any given jump, but you can jump several pieces with a move\n" +
				"of several jumps. You remove the jumped pieces from the board. You cannot jump your own piece.\n" +
				"You cannot jump the same piece twice, in the same move. If you can jump, you must.\n" +
				"And, a multiple jump must be completed; you cannot stop part way through a multiple jump.\n" +
				"If you have a choice of jumps, you can choose among them, regardless of whether some of them\n" +
				"are multiple, or not. A piece, whether it is a king or not, can jump a king.";
		this.tutorialTexts.add(page7);
		
		ImageIcon img7 = CheckersImageCollection.getInstance().getCheckersMultiCaptureIcon();
		this.tutorialImages.add(img7);
		
		// Page 8, Kinging
		String header8 = "King Promotions (8 / 8)";
		this.headerTexts.add(header8);
		
		String page8 = "When a piece reaches the last row (the King Row), it becomes a King.\n" +
				"In the real world, a second checker is placed on top of that one, by the opponent.\n" +
				"In this game a \"K\" will appear on the Checkers piece which just got promoted to a king.\n" +
				"A piece that has just been promoted to a king, cannot continue jumping pieces, until the next move.";
		this.tutorialTexts.add(page8);
		
		ImageIcon img8 = CheckersImageCollection.getInstance().getCheckersKingsIcon();
		this.tutorialImages.add(img8);
	}
}
