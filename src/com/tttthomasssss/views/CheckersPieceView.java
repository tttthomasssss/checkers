package com.tttthomasssss.views;

import java.awt.Dimension;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import com.tttthomasssss.defaults.CheckersDefaults.PieceColour;
import com.tttthomasssss.defaults.CheckersDefaults.PieceIdentity;
import com.tttthomasssss.defaults.CheckersImageCollection;
import com.tttthomasssss.interfaces.CheckersPieceViewDelegate;
import com.tttthomasssss.models.CheckersPosition;

public class CheckersPieceView 
extends JLabel 
implements DragGestureListener, DragSourceListener, Transferable, Serializable {
	
	private static final long serialVersionUID = 3958980753274726793L;
	public static final int PREFERRED_WIDTH 	= 64;
	public static final int PREFERRED_HEIGHT	= 64;
	
	private PieceColour 		colour;
	private PieceIdentity 		identity;
	private CheckersPosition	currentPosition;
	
	// 25/11/13
	// DragNDropStuff
	private DragSource			dragSource;
	
	private CheckersPieceViewDelegate delegate;
	
	// 25/11/13
	//private DragSourceListener delegate;
	
	private ArrayList<Observer> observers;
	
	// Transferable Stuff
	public static final DataFlavor CHECKERS_FLAVOUR = new DataFlavor(CheckersPieceView.class, "A Checkers Piece View Object");
	private static DataFlavor[] SUPPORTED_FLAVOURS = {CHECKERS_FLAVOUR};
	
	public CheckersPieceView(PieceColour colour, PieceIdentity identity, CheckersPosition currentPosition)
	{
		super();
		
		this.colour 			= colour;
		this.identity			= identity;
		this.currentPosition	= currentPosition;
		this.observers			= new ArrayList<Observer>();
		// 25/11/13 the 797897th try
		//this.dragSource			= new DragSource();
		//this.dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_MOVE, this);
		
		// 25/11/13 the 797897th try
		this.dragSource = new DragSource();
		this.dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_MOVE, this);// or this.dgListener
		//this.dgListener = new CheckersDragGestureListener();
		//this.dsListener = new CheckersDragSourceListener();
		
		super.setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));
		super.setIcon(CheckersImageCollection.getInstance().getPieceIconForColourAndIdentity(this.colour, this.identity));
		super.setDisabledIcon(CheckersImageCollection.getInstance().getPieceIconForColourAndIdentity(this.colour, this.identity));
		
		// 25/11/13 the 797897th try
		//super.setTransferHandler(new TransferHandler("icon"));
	}
	
	// 25/11/13
	
	public void setDelegate(CheckersPieceViewDelegate delegate)
	{
		this.delegate = delegate;
	}
	
	public CheckersPosition getCurrentPosition()
	{
		return this.currentPosition;
	}
	
	public void setCurrentPosition(CheckersPosition pos)
	{
		this.currentPosition = pos;
	}
	
	public PieceColour getPieceColour()
	{
		return this.colour;
	}
	
	public void promoteToKing()
	{
		if (this.identity != PieceIdentity.KING) {
			this.identity = PieceIdentity.KING;
			super.setIcon(CheckersImageCollection.getInstance().getKingIconForColour(this.colour));
			super.setDisabledIcon(CheckersImageCollection.getInstance().getKingIconForColour(this.colour));
		}
	}
	
	@Override
	public void dragGestureRecognized(DragGestureEvent dge) 
	{
		
		if (this.isEnabled()) {
			this.dragSource.startDrag(dge, DragSource.DefaultMoveDrop, this, this);
		} else {
			System.out.println("NOT ENABLED");
		}
		// 25/11/13
		//dge.sta
		/*
		if (this.isEnabled()) {
			dge.startDrag(DragSource.DefaultMoveDrop, this, this);
		} else {
			System.out.println("NOT ENABLED!!!");
		}*/
	}

	@Override
	public void dragEnter(DragSourceDragEvent dsde) 
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void dragOver(DragSourceDragEvent dsde) 
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void dropActionChanged(DragSourceDragEvent dsde) 
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void dragExit(DragSourceEvent dse) 
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void dragDropEnd(DragSourceDropEvent dsde) 
	{
		// TODO Auto-generated method stub
		this.delegate.moveFinished();
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() 
	{
		return CheckersPieceView.SUPPORTED_FLAVOURS;
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor) 
	{
		return flavor.equals(CheckersPieceView.CHECKERS_FLAVOUR);
	}

	@Override
	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException
	{
		if (this.isDataFlavorSupported(flavor)) {
			return this;
		} else {
			throw new UnsupportedFlavorException(flavor);
		}
	}

}
