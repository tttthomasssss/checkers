package com.tttthomasssss.interfaces;

import com.tttthomasssss.defaults.CheckersDefaults.PieceColour;
import com.tttthomasssss.models.CheckersPosition;

public interface CheckersTileViewDelegate {
	
	public PieceColour getPlayerColour();
	public void validateMove(CheckersPosition from, CheckersPosition to);
	public void movePiece(CheckersPosition from, CheckersPosition to);
	public void moveFinished();
	
}
