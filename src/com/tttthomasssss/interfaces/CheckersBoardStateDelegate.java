package com.tttthomasssss.interfaces;

import com.tttthomasssss.models.CheckersPosition;

public interface CheckersBoardStateDelegate {
	
	public void capturePieceAt(CheckersPosition pos);
	public void promotePieceToKingForMove(CheckersPosition from, CheckersPosition to);
	public void updatePieceCount(int whitePiecesLeft, int blackPiecesLeft);
}
