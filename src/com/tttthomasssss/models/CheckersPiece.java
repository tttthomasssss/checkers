package com.tttthomasssss.models;

import java.io.Serializable;

import com.tttthomasssss.defaults.CheckersDefaults.PieceColour;
import com.tttthomasssss.defaults.CheckersDefaults.PieceIdentity;

public class CheckersPiece 
implements Cloneable, Serializable
{
	
	private static final long serialVersionUID = 3562631798384627218L;
	private boolean			isBeaten;
	private PieceIdentity	identity;
	private PieceColour		colour;
	
	public CheckersPiece(PieceIdentity identity, PieceColour colour)
	{
		super();
		
		this.identity 	= identity;
		this.colour		= colour;
		this.isBeaten 	= false;
	}

	public boolean isBeaten() 
	{
		return this.isBeaten;
	}

	public void setBeaten(boolean beaten)
	{
		this.isBeaten = beaten;
	}
	
	public PieceIdentity getIdentity() 
	{
		return this.identity;
	}
	
	public boolean isKing()
	{
		return this.identity == PieceIdentity.KING;
	}
	
	public boolean isWhite()
	{
		return this.colour == PieceColour.WHITE;
	}
	
	public boolean isEnemyColour(PieceColour colour)
	{
		return this.colour != colour;
	}
	
	public boolean isEnemyPiece(CheckersPiece other)
	{
		return this.colour != other.colour;
	}
	
	public PieceColour getColour()
	{
		return this.colour;
	}
	
	public void promoteToKing()
	{
		this.identity = PieceIdentity.KING;
	}
	
	public String toString()
	{
		String str = "";
		if (!this.isBeaten) {
			str += this.colour.getValue();
			if (this.identity == PieceIdentity.KING) {
				str += "*";
			} else {
				str += " ";
			}
		} else {
			str += "  ";
		}
		return str;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colour == null) ? 0 : colour.hashCode());
		result = prime * result
				+ ((identity == null) ? 0 : identity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckersPiece other = (CheckersPiece) obj;
		if (colour != other.colour)
			return false;
		if (identity != other.identity)
			return false;
		return true;
	}
	
	public CheckersPiece clone()
	{
		CheckersPiece theCopy = null;
		
		try {
			theCopy = (CheckersPiece) super.clone();
			theCopy.identity = this.identity;
			theCopy.colour = this.colour;
		} catch (CloneNotSupportedException ex) {
			ex.printStackTrace();
		}
		
		return theCopy;
	}
}
