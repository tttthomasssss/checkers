package com.tttthomasssss.models;

import java.io.Serializable;
import java.util.*;

import com.tttthomasssss.controllers.ValidationController;
import com.tttthomasssss.defaults.CheckersDefaults;
import com.tttthomasssss.defaults.CheckersUtils;
import com.tttthomasssss.defaults.CheckersDefaults.PieceColour;
import com.tttthomasssss.defaults.CheckersDefaults.PieceIdentity;
import com.tttthomasssss.interfaces.CheckersBoardStateDelegate;

public class CheckersBoardState 
implements Cloneable, Comparable<CheckersBoardState>, Serializable {
	
	private static final long serialVersionUID = 6690744857930573189L;
	// TODO: The validation controller is not really pretty here...
	private ValidationController validationController;
	private int rows;
	private int cols;
	private int nrOfWhitePieces;
	private int nrOfBlackPieces;
	private int nrOfWhiteKings;
	private int nrOfBlackKings;
	private int whiteScore;
	private int blackScore;
	private PieceColour playerColour;
	private CheckersTile[][] gameBoard;
	private CheckersMoveSequence moveSequence;
	private CheckersBoardState parent;
	private int evaluation;
	private boolean evalSet;
	private CheckersBoardStateDelegate delegate;
	
	public CheckersBoardState()
	{
		this(CheckersDefaults.DFLT_NR_OF_ROWS, CheckersDefaults.DFLT_NR_OF_COLS, PieceColour.WHITE);
	}
	
	public CheckersBoardState(PieceColour playerColour)
	{
		this(CheckersDefaults.DFLT_NR_OF_ROWS, CheckersDefaults.DFLT_NR_OF_COLS, playerColour);
	}
	
	public CheckersBoardState(int rows, int cols, PieceColour playerColour)
	{
		super();
		// TODO: Check the inputs!
		this.rows = rows;
		this.cols = cols;
		this.nrOfWhitePieces = CheckersDefaults.DFLT_NR_OF_PIECES;
		this.nrOfBlackPieces = CheckersDefaults.DFLT_NR_OF_PIECES;
		this.nrOfWhiteKings = 0;
		this.nrOfBlackKings = 0;
		this.gameBoard = new CheckersTile[this.rows][this.cols];
		this.validationController = ValidationController.getInstance();
		this.moveSequence = new CheckersMoveSequence(this.validationController);
		this.playerColour = playerColour;
		this.parent = null;
		this.evaluation = 0; // Integer.MIN_VALUE;
		this.evalSet = false;
		
		// For Testing use some other GameBoard
		this.initStartingPositionGameBoard();
		//this.initYetAnotherMultiJumpGameBoard();
		//this.initMultiJumpGameBoard();
		//this.initAnotherMultiJumpGameBoard();
		//this.initMultiJumpKingBoard();
		//this.initKingPromotionBoard();
		//this.initMinimaxBoard();
		//this.initMinimalistNullPointerExceptionBoard();
		//this.initAlphaBetaPruningBoard();
		//this.initYetAnotherFailBoard();
		
		this.updateGameScore();
	}
	
	public boolean hasPlayerOpenCapturesFromPosition(CheckersPosition pos)
	{
		return this.validationController.hasOpenCapturesFromPosition(pos, this);
	}
	
	public CheckersBoardState movePiece(CheckersPosition from, CheckersPosition to, boolean isCapture, boolean isMulti, boolean forceCaptures)
	{
		CheckersBoardState newState = null;
		
		if (this.validationController.validPlayerMove(from, to, this, forceCaptures)) {
			newState = this.produceSuccessorByMovingPiece(from, to, isCapture, isMulti);
			
			// Update the view
			if (isCapture) {
				this.delegate.capturePieceAt(CheckersUtils.getCapturedPiecePosition(from, to));
				this.delegate.updatePieceCount(newState.getWhitePiecesLeft(), newState.getBlackPiecesLeft());
			}
			
			// Make an unsafe redundant check here, if the piece was already a king, the view won't do anything
			// Necessary because the successor has already been produced, thus there is already a king at the to position now
			// That means that the check for canPromoteToKing will fail
			if (CheckersUtils.canPromoteToKingAllowRedundant(to, newState)) {
				this.delegate.promotePieceToKingForMove(from, to);
			}
			
		}
		
		return newState;
	}
	
	public CheckersBoardStateDelegate getDelegate()
	{
		return this.delegate;
	}
	
	public void setDelegate(CheckersBoardStateDelegate delegate)
	{
		this.delegate = delegate;
	}
	
	public int getWhitePiecesLeft()
	{
		return this.nrOfWhiteKings + this.nrOfWhitePieces;
	}
	
	public int getBlackPiecesLeft()
	{
		return this.nrOfBlackKings + this.nrOfBlackPieces;
	}
	
	public CheckersMoveSequence getMoveSequence()
	{
		return this.moveSequence;
	}
	
	public int getRows() 
	{
		return this.rows;
	}

	public int getCols() 
	{
		return this.cols;
	}
	
	public boolean isEvalSet()
	{
		return this.evalSet;
	}
	
	public int getEvaluation()
	{
		return this.evaluation;
	}
	
	public void setEvaluation(int evaluation)
	{
		this.evaluation = evaluation;
		this.evalSet = true;
	}
	
	public CheckersBoardState getParent()
	{
		return this.parent;
	}
	
	public int getGameScore()
	{
		return (this.playerColour == PieceColour.WHITE) ? this.whiteScore - this.blackScore : this.blackScore - this.whiteScore;
	}
	
	public int getScoreForColour(PieceColour colour)
	{
		return (colour == PieceColour.WHITE) ? this.whiteScore : this.blackScore;
	}
	
	public boolean isWhiteAt(CheckersPosition pos)
	{	
		return /*!this.isEmptyAt(pos) && */this.gameBoard[pos.getRow()][pos.getCol()].getPiece().isWhite();
	}
	
	public boolean isKingAt(CheckersPosition pos)
	{
		return !this.isEmptyAt(pos) && this.gameBoard[pos.getRow()][pos.getCol()].getPiece().isKing();
	}
	
	public boolean isEmptyAt(CheckersPosition pos)
	{
		return this.gameBoard[pos.getRow()][pos.getCol()].getPiece() == null;
	}
	
	public CheckersPiece getPieceAt(CheckersPosition pos)
	{
		if (!pos.positionOutOfBounds()) {
			return this.gameBoard[pos.getRow()][pos.getCol()].getPiece();
		} 
		
		return null;
	}
	
	public CheckersTile getTileAt(CheckersPosition pos)
	{
		return this.gameBoard[pos.getRow()][pos.getCol()];
	}
	
	public void promotePieceToKingAt(CheckersPosition pos)
	{
		this.getPieceAt(pos).promoteToKing();
		if (this.isWhiteAt(pos)) {
			this.nrOfWhiteKings++;
			this.nrOfWhitePieces--;
		} else {
			this.nrOfBlackKings++;
			this.nrOfBlackPieces--;
		}
		
		//this.updateGameScore();
	}
	
	public void capturePieceAt(CheckersPosition pos)
	{
		if (this.isWhiteAt(pos)) {
			if (this.isKingAt(pos)) {
				this.nrOfWhiteKings--;
			} else {
				this.nrOfWhitePieces--;
			} 
		} else {
			if (this.isKingAt(pos)) {
				this.nrOfBlackKings--;
			} else {
				this.nrOfBlackPieces--;
			}
		}
		//this.updateGameScore();
		this.gameBoard[pos.getRow()][pos.getCol()].setPiece(null);// = new CheckersTile();
	}
	
	public void printGameBoard()
	{
		for (int i = 0; i < this.rows; i++) {
			for (int j = 0; j < this.cols; j++) {
				System.out.print(this.gameBoard[i][j]);
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public String toString()
	{
		String str = "";
		for (int i = 0; i < this.rows; i++) {
			for (int j = 0; j < this.cols; j++) {
				str += this.gameBoard[i][j];
			}
			str += "\n";
		}
		
		return str;
	}
	
	private void initMultiJumpGameBoard()
	{
		this.initEmptyGameBoard();
		
		// Setup black pieces
		this.gameBoard[2][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[2][3] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[2][5] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		
		this.gameBoard[4][3] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		//this.gameBoard[4][5] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		
		this.gameBoard[6][3] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[6][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		
		// Setup white piece
		this.gameBoard[7][2] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
	
		this.nrOfBlackPieces = 6;
		this.nrOfWhitePieces = 1;
		
	}
	
	private void initKingPromotionBoard()
	{
		this.initEmptyGameBoard();
		
		// Setup black pieces
		this.gameBoard[6][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		
		// Setup white pieces
		this.gameBoard[1][0] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		
		this.nrOfBlackPieces = 1;
		this.nrOfWhitePieces = 1;
	}
	
	private void initAnotherMultiJumpGameBoard()
	{
		this.initEmptyGameBoard();
		
		// Setup black pieces
		this.gameBoard[2][3] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
				
		this.gameBoard[4][3] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
				
		this.gameBoard[6][3] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[6][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
				
		// Setup white piece
		this.gameBoard[1][2] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[2][7] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[7][2] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
			
		this.nrOfBlackPieces = 4;
		this.nrOfWhitePieces = 3;
	}
	
	private void initYetAnotherMultiJumpGameBoard()
	{
		this.initEmptyGameBoard();
		
		// Setup black pieces
		this.gameBoard[2][5] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[4][5] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[6][3] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[6][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[4][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		
		// Setup white piece
		this.gameBoard[1][0] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[1][2] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[2][7] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[7][2] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
			
		this.nrOfBlackPieces = 5;
		this.nrOfWhitePieces = 4;
	}
	
	private void initMultiJumpKingBoard()
	{
		this.initEmptyGameBoard();
		
		// Setup black pieces
		this.gameBoard[2][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[2][3] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[2][5] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		
		this.gameBoard[4][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[4][3] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[4][5] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		
		this.gameBoard[6][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[6][3] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[6][5] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		
		// Setup white pieces
		this.gameBoard[7][2] = new CheckersTile(new CheckersPiece(PieceIdentity.KING, PieceColour.WHITE));
		
		this.nrOfBlackPieces = 9;
		this.nrOfWhitePieces = 0;
		this.nrOfWhiteKings = 1;
	}
	
	private void initMinimaxBoard()
	{
		this.initEmptyGameBoard();
		
		// Setup Black Pieces
		this.gameBoard[0][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[2][3] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[5][6] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[6][3] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		
		// Setup White Pieces
		this.gameBoard[3][0] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[3][4] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[5][2] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[6][7] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[7][0] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		
		this.nrOfBlackPieces = 4;
		this.nrOfWhitePieces = 5;
	}
	
	private void initMinimalistNullPointerExceptionBoard()
	{
		this.initEmptyGameBoard();
		
		// Setup Black Pieces
		this.gameBoard[0][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[5][6] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		
		// Setup White Pieces
		this.gameBoard[3][0] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[3][4] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		
		this.nrOfBlackPieces = 2;
		this.nrOfWhitePieces = 2;
	}
	
	private void initYetAnotherFailBoard()
	{
		this.initEmptyGameBoard();
		
		// Setup Black Pieces
		this.gameBoard[7][6] = new CheckersTile(new CheckersPiece(PieceIdentity.KING, PieceColour.BLACK));
		this.gameBoard[0][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
				
		// Setup White Pieces
		this.gameBoard[1][2] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[2][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
				
		
		this.nrOfBlackKings = 1;
		this.nrOfBlackPieces = 1;
		this.nrOfWhitePieces = 2;
	}
	
	private void initAnotherMinimaxBoard()
	{
		this.initEmptyGameBoard();
		
		// Setup Black Pieces
		this.gameBoard[6][5] = new CheckersTile(new CheckersPiece(PieceIdentity.KING, PieceColour.BLACK));
		
		// Setup White Pieces
		this.gameBoard[1][6] = new CheckersTile(new CheckersPiece(PieceIdentity.KING, PieceColour.BLACK));
		this.gameBoard[4][7] = new CheckersTile(new CheckersPiece(PieceIdentity.KING, PieceColour.BLACK));
		
		this.nrOfBlackKings = 1;
		this.nrOfBlackPieces = 0;
		this.nrOfWhiteKings = 2;
		this.nrOfWhitePieces = 0;
	}
	
	private void initAlphaBetaPruningBoard()
	{
		this.initEmptyGameBoard();
		
		// Setup Black Pieces
		this.gameBoard[1][4] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[1][7] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[3][0] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[3][2] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[4][5] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		this.gameBoard[6][2] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
		
		// Setup White Pieces
		this.gameBoard[2][6] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[4][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[5][4] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		this.gameBoard[7][1] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
		
		this.nrOfBlackPieces = 6;
		this.nrOfWhitePieces = 4;
	}
	
	private void initStartingPositionGameBoard()
	{
		// TODO: Introduce HUMAN_PLAYER_IS_WHITE flag to indicate which colour the player plays
		this.initEmptyGameBoard();
		
		int start = 1;
		
		// Setup Computer controlled pieces
		for (int i = 0; i < 3; i++) {
			start = Math.abs((i % 2) - 1);
			for (int j = start; j < this.cols; j += 2) {
				this.gameBoard[i][j] = new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.BLACK));
			}
		}
		
		start = 0;
		
		// Setup Human controlled pieces
		for (int i = this.rows - 1; i > this.rows - 4; i--) {
			start = Math.abs((i % 2) - 1);
			for (int j = start; j < this.cols; j+= 2) {
				this.gameBoard[i][j] =  new CheckersTile(new CheckersPiece(PieceIdentity.NORMAL, PieceColour.WHITE));
			}
		}
	}
	
	private void initEmptyGameBoard()
	{
		for (int i = 0; i < this.rows; i++) {
			for (int j = 0; j < this.cols; j++) {
				this.gameBoard[i][j] = new CheckersTile();
			}
		}
	}

	// Implementation of BoardState Interface
	// TODO: Maybe do an abstract base class instead of the interface which is currently unused anyway
	@Override
	public int compareTo(CheckersBoardState o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/*
	public CheckersTile[][] produceGameBoardSuccessorByMovingPiece(CheckersPosition from, CheckersPosition to)
	{
		CheckersTile[][] gameBoardCopy = null;
		
		// Check if the move is going to produce a valid successor
		if (this.validationController.validMove(from, to, this)) {

			// Copy the stuff
			gameBoardCopy = CheckersBoardUtils.deepClone(this.gameBoard);
			
			gameBoardCopy[to.getRow()][to.getCol()] = this.gameBoard[from.getRow()][from.getCol()];
			gameBoardCopy[from.getRow()][from.getCol()] = new CheckersTile();
		}
		return gameBoardCopy;
	}*/
	
	
	public void resetMoveSequence()
	{
		this.moveSequence = new CheckersMoveSequence(this.validationController);
	}
	
	private CheckersBoardState produceSuccessorByMovingPiece(CheckersPosition from, CheckersPosition to, boolean isCapture, boolean isMulti)
	{
		CheckersBoardState newState = this.clone();
		
		CheckersPiece copy = null;
		
		copy = this.gameBoard[from.getRow()][from.getCol()].getPiece().clone();
		
		newState.gameBoard[to.getRow()][to.getCol()].setPiece(copy);
		newState.gameBoard[from.getRow()][from.getCol()].setPiece(null);// = new CheckersTile();
		
		// 26/11/2013: If it is a multicapture, ie. an ongoing operation, append the new move, otherwise re-initialise
		if (!isMulti) {
			newState.moveSequence = new CheckersMoveSequence(this.validationController);
		} 
		newState.moveSequence.addMove(from, to, this);
		
		newState.parent = this;
		
		newState.evaluation = Integer.MIN_VALUE;
		
		// Remove beaten piece from board
		if (isCapture) {
			newState.capturePieceAt(CheckersUtils.getCapturedPiecePosition(from, to));
		}
		
		// King conversion
		if (CheckersUtils.canPromoteToKing(to, newState)) {
			newState.promotePieceToKingAt(to);
		}
		
		// Explicitely update the Game Score
		newState.updateGameScore();
		
		return newState;
	}
	
	public ArrayList<CheckersBoardState> getSuccessors(PieceColour colour, boolean forceCaptures)
	{
		ArrayList<CheckersBoardState> successors = new ArrayList<CheckersBoardState>();
		ArrayList<CheckersBoardState> normalMoveSuccessors = new ArrayList<CheckersBoardState>();
		
		for (int i = 0; i < CheckersDefaults.DFLT_NR_OF_ROWS; i++) {
			for (int j = 0; j < CheckersDefaults.DFLT_NR_OF_COLS; j++) {
				if (this.gameBoard[i][j].getPiece() != null && this.gameBoard[i][j].getPiece().getColour() == colour) {
					
					// Differentiate colour
					int rowHelper = this.gameBoard[i][j].getPiece().isWhite() ? -1 : 1;
					
					CheckersPosition currPos = new CheckersPosition(i, j);
					
					//-- Normal moves --//
					CheckersPosition newPos = new CheckersPosition(i + rowHelper, j + 1);
					if (this.validationController.validMove(currPos, newPos, this)) {
						normalMoveSuccessors.add(this.produceSuccessorByMovingPiece(currPos, newPos, false, false));
					}
					
					newPos = new CheckersPosition(i + rowHelper, j - 1);
					if (this.validationController.validMove(currPos, newPos, this)) {
						normalMoveSuccessors.add(this.produceSuccessorByMovingPiece(currPos, newPos, false, false));
					}
					
					//-- Normal king moves --//
					if (this.gameBoard[i][j].getPiece().isKing()) {
						newPos = new CheckersPosition(i - rowHelper, j + 1);
						if (this.validationController.validMove(currPos, newPos, this)) {
							normalMoveSuccessors.add(this.produceSuccessorByMovingPiece(currPos, newPos, false, false));
						}
						
						newPos = new CheckersPosition(i - rowHelper, j - 1);
						if (this.validationController.validMove(currPos, newPos, this)) {
							normalMoveSuccessors.add(this.produceSuccessorByMovingPiece(currPos, newPos, false, false));
						}
					}
					
					//-- Jumps --//
					ArrayList<CheckersBoardState> multiJumpSuccessors = this.createMultiJumpSuccessors(currPos, rowHelper);
					
					if (multiJumpSuccessors.size() > 0) {
						successors.addAll(multiJumpSuccessors);
						
					}
				}
			}
		}
		
		// Enforce Jumps, if successor list is non-empty, don't produce any other successors
		if ((!forceCaptures || successors.size() <= 0) && normalMoveSuccessors.size() > 0) {
			successors.addAll(normalMoveSuccessors);
		}	
		
		return successors;
	}
	
	private void updateGameScore()
	{
		/* probably can use this again
		if (this.isWinningStateForColour(PieceColour.WHITE)) {
			this.whiteScore = CheckersDefaults.WINNING_STATE;
			this.blackScore = CheckersDefaults.LOSING_STATE;
		} else if (this.isWinningStateForColour(PieceColour.BLACK)) {
			this.whiteScore = CheckersDefaults.LOSING_STATE;
			this.blackScore = CheckersDefaults.WINNING_STATE;
		} else {
			
			int whitePieceSign = this.nrOfWhitePieces >= this.nrOfBlackPieces ? 1 : -1;
			int blackPieceSign = -whitePieceSign;
			int whiteKingSign = this.nrOfWhiteKings >= this.nrOfBlackKings ? 1 : -1;
			int blackKingSign = -whiteKingSign;
			
			int whitePieceScore = whitePieceSign * (int)Math.pow((this.nrOfWhitePieces - this.nrOfBlackPieces), 2);
			int whiteKingScore = whiteKingSign * (int)Math.pow(((this.nrOfWhiteKings - this.nrOfBlackKings) * CheckersDefaults.KING_BONUS), 2);
			
			int blackPieceScore = blackPieceSign * (int)Math.pow((this.nrOfBlackPieces - this.nrOfWhitePieces), 2);
			int blackKingScore = blackKingSign * (int)Math.pow(((this.nrOfBlackKings - this.nrOfWhiteKings) * CheckersDefaults.KING_BONUS), 2);
			
			this.whiteScore = Math.max(whitePieceScore + whiteKingScore, 0);//+ ((this.nrOfWhiteKings + this.nrOfWhitePieces - this.nrOfBlackKings - this.nrOfBlackPieces) * CheckersDefaults.CAPTURE_BONUS);
			this.blackScore = Math.max(blackPieceScore + blackKingScore, 0);//+ ((this.nrOfBlackKings + this.nrOfBlackPieces - this.nrOfWhiteKings - this.nrOfWhitePieces) * CheckersDefaults.CAPTURE_BONUS);
		}*/
		
		if (this.isWinningStateForColour(PieceColour.WHITE)) {
			this.whiteScore = CheckersDefaults.WINNING_STATE;
			this.blackScore = CheckersDefaults.LOSING_STATE_MINIMAX;
		} else if (this.isWinningStateForColour(PieceColour.BLACK)) {
			this.whiteScore = CheckersDefaults.LOSING_STATE_MINIMAX;
			this.blackScore = CheckersDefaults.WINNING_STATE;
		} else {
			int deltaWhitePieces = (this.nrOfWhitePieces - this.nrOfBlackPieces);
			int deltaWhiteKings = (this.nrOfWhiteKings - this.nrOfBlackKings) * CheckersDefaults.KING_BONUS;
			int deltaBlackPieces = (this.nrOfBlackPieces - this.nrOfWhitePieces);
			int deltaBlackKings = (this.nrOfBlackKings - this.nrOfWhiteKings) * CheckersDefaults.KING_BONUS;
		
			// NegMax
			//this.whiteScore = Math.max(deltaWhitePieces + deltaWhiteKings, 0);
			//this.blackScore = Math.max(deltaBlackPieces + deltaBlackKings, 0);
			
			// MiniMax
			this.whiteScore = deltaWhitePieces + deltaWhiteKings;
			this.blackScore = deltaBlackPieces + deltaBlackKings;
		}
	}
	
	public boolean isWinningStateForColour(PieceColour colour)
	{
		//boolean opponentCantMove = successors == null || successors.size() <= 0;
		return (colour == PieceColour.WHITE) ? ((this.nrOfBlackKings + this.nrOfBlackPieces) == 0) : ((this.nrOfWhiteKings + this.nrOfWhitePieces) == 0);
	}
	
	public boolean isLosingStateForColour(PieceColour colour)
	{
		return (colour == PieceColour.WHITE) ? ((this.nrOfWhiteKings + this.nrOfWhitePieces) == 0) : ((this.nrOfBlackKings + this.nrOfBlackPieces) == 0);
	}
	
	@Override
	public CheckersBoardState clone()
	{
		CheckersBoardState theCopy = null;
		
		try {
			theCopy = (CheckersBoardState) super.clone();
			theCopy.gameBoard = CheckersBoardUtils.deepClone(this.gameBoard);
			theCopy.moveSequence = this.moveSequence.clone();//new CheckersMoveSequence(this.validationController);//this.moveSequence.clone();
			theCopy.nrOfWhiteKings = this.nrOfWhiteKings;
			theCopy.nrOfWhitePieces = this.nrOfWhitePieces;
			theCopy.nrOfBlackKings = this.nrOfBlackKings;
			theCopy.nrOfBlackPieces = this.nrOfBlackPieces;
			theCopy.playerColour = this.playerColour;
			theCopy.whiteScore = this.whiteScore;
			theCopy.blackScore = this.blackScore;
			
			// We fail and fail and fail
			theCopy.evaluation = 0;
			theCopy.evalSet = false;
			
		} catch (CloneNotSupportedException ex) {
			ex.printStackTrace();
		}
		
		return theCopy;
	}
	
	public boolean equals(Object o)
	{ 
		boolean equals = false;

		if (o != null) {
			if (o == this) {
				equals = true;
			} else {
				if (o instanceof CheckersBoardState) {
					CheckersBoardState other = (CheckersBoardState)o;

					// TODO: Also take num of pieces per colour into account!
					if (
							Arrays.deepEquals(this.gameBoard, other.gameBoard) 	&&
							this.nrOfWhiteKings == other.nrOfWhiteKings			&&
							this.nrOfWhitePieces == other.nrOfWhitePieces		&&
							this.nrOfBlackKings == other.nrOfBlackKings			&&
							this.nrOfBlackPieces == other.nrOfBlackPieces		&&
							this.playerColour == other.playerColour
					) {
						equals = true;
					}
				}
			}
		}

		return equals;
	}
	
	public int hashCode()
	{ 
		int result = 17;

		result = 31 * result + Arrays.deepHashCode(this.gameBoard);
		result = 31 * result + this.nrOfWhiteKings;
		result = 31 * result + this.nrOfWhitePieces;
		result = 31 * result + this.nrOfBlackKings;
		result = 31 * result + this.nrOfBlackPieces;
		result = 31 * result + this.playerColour.getValue();
		
		return result;
	}
	
	public void printRemainingPieceNumbers()
	{
		System.out.println("Overall Number of white pieces: " + (this.nrOfWhiteKings + this.nrOfWhitePieces));
		System.out.println("Number of white normal pieces: " + this.nrOfWhitePieces);
		System.out.println("Number of white kings: " + this.nrOfWhiteKings);
		System.out.println("Overall Number of black pieces: " + (this.nrOfBlackKings + this.nrOfBlackPieces));
		System.out.println("Number of black normal pieces: " + this.nrOfBlackPieces);
		System.out.println("Number of black kings: " + this.nrOfBlackKings);
	}
	
	private ArrayList<CheckersBoardState> createMultiJumpSuccessors(CheckersPosition oldPos, int rowHelper)
	{
		Stack<CheckersBoardState> openList = new Stack<CheckersBoardState>();
		HashSet<CheckersBoardState> alreadySeen = new HashSet<CheckersBoardState>();
		
		ArrayList<CheckersBoardState> leafNodes = new ArrayList<CheckersBoardState>();
		
		// Produce first 2 moves (jump left & jump right) & push to Stack
		CheckersPosition newPos = new CheckersPosition(oldPos.getRow() + (2 * rowHelper), oldPos.getCol() + 2);
		if (this.validationController.validMove(oldPos, newPos, this)) {
			openList.push(this.produceSuccessorByMovingPiece(oldPos, newPos, true, false));
		}
		
		newPos = new CheckersPosition(oldPos.getRow() + (2 * rowHelper), oldPos.getCol() - 2);
		if (this.validationController.validMove(oldPos, newPos, this)) {
			openList.push(this.produceSuccessorByMovingPiece(oldPos, newPos, true, false));
		}
		
		// First 2 King moves (jump left back & jump right back) & push to Stack
		// ad Issue (https://bitbucket.org/tttthomasssss/checkers/issue/2/king-promotions-multijumps):
		// Check here if the possible new state has an empty move sequence
		// if so, it can be added because it has been a king prior to this move
		// if not, it must have just gotten promoted and is not allowed to move on
		if (this.isKingAt(oldPos)) {
			newPos = new CheckersPosition(oldPos.getRow() - (2 * rowHelper), oldPos.getCol() + 2);
			if (this.validationController.validMove(oldPos, newPos, this)) {
				openList.push(this.produceSuccessorByMovingPiece(oldPos, newPos, true, false));
			}
			
			newPos = new CheckersPosition(oldPos.getRow() - (2 * rowHelper), oldPos.getCol() - 2);
			if (this.validationController.validMove(oldPos, newPos, this)) {
				openList.push(this.produceSuccessorByMovingPiece(oldPos, newPos, true, false));
			}
		}
		
		
		CheckersBoardState currentState = null;
		
		boolean newStates = false;
		
		while (!openList.isEmpty()) {
			
			currentState = openList.pop();
			newStates = false;
			
			if (!alreadySeen.contains(currentState)) {
			
				alreadySeen.add(currentState);
				
				CheckersPosition currPos = currentState.moveSequence.peekLastPosition();
				
				//-- Check jump successors for current state --//
			
				// currCol + 2
				newPos = new CheckersPosition(currPos.getRow() + (2 * rowHelper), currPos.getCol() + 2);
				if (ValidationController.getInstance().validMove(currPos, newPos, currentState)) {
					CheckersBoardState newState = currentState.produceSuccessorByMovingPiece(currPos, newPos, true, true);
					openList.push(newState);
					newStates = true;
				}
			
				// currCol - 2
				newPos = new CheckersPosition(currPos.getRow() + (2 * rowHelper), currPos.getCol() - 2);
				if (ValidationController.getInstance().validMove(currPos, newPos, currentState)) {
					CheckersBoardState newState = currentState.produceSuccessorByMovingPiece(currPos, newPos, true, true);
					openList.push(newState);
					newStates = true;
				}
				
				//-- King jump successors for current state --//
				if (currentState.isKingAt(currPos)) {
					
					// currCol + 2
					newPos = new CheckersPosition(currPos.getRow() - (2 * rowHelper), currPos.getCol() + 2);
					if (ValidationController.getInstance().validMove(currPos, newPos, currentState)) {
						CheckersBoardState newState = currentState.produceSuccessorByMovingPiece(currPos, newPos, true, true);
						openList.push(newState);
						newStates = true;
					}
					
					// currCol - 2
					newPos = new CheckersPosition(currPos.getRow() - (2 * rowHelper), currPos.getCol() - 2);
					if (ValidationController.getInstance().validMove(currPos, newPos, currentState)) {
						CheckersBoardState newState = currentState.produceSuccessorByMovingPiece(currPos, newPos, true, true);
						openList.push(newState);
						newStates = true;
					}
				}
				
				if (!newStates) {
					leafNodes.add(currentState);
				}
			}
		}
		
		return leafNodes;
	}
	
	private void traceGameBoards()
	{
		CheckersBoardState s = this;
		
		while (s != null) {
			s.printGameBoard();
			System.out.println();
			System.out.println("MOVE SEQ: " + s.getMoveSequence());
			System.out.println("---------------------------------------------------------");
			s = s.getParent();
		}
	}
	
	// Private Utility Class for deep Cloning the 2d Boardstate array
	private static class CheckersBoardUtils
	{
		/**
		 * Returns a deepClone of the 2d Array.
		 * @param theArray to be cloned.
		 * @return a cloned Array.
		 */
		private static CheckersTile[][] deepClone(CheckersTile[][] theArray)
		{
			CheckersTile[][] theClone = new CheckersTile[CheckersDefaults.DFLT_NR_OF_ROWS][];

			for (int i = 0; i < theArray.length; i++) {
				theClone[i] = Arrays.copyOf(theArray[i], theArray[i].length);
			}
			
			// Clone contents
			for (int i = 0; i < CheckersDefaults.DFLT_NR_OF_ROWS; i++) {
				for (int j = 0; j < CheckersDefaults.DFLT_NR_OF_COLS; j++) {
					theClone[i][j] = theArray[i][j].clone();
				}
			}
			
			return theClone;
		}
	}
}
