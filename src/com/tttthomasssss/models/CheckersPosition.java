package com.tttthomasssss.models;

import java.io.Serializable;

import com.tttthomasssss.defaults.CheckersDefaults;

public class CheckersPosition implements Cloneable, Serializable {
	
	private static final long serialVersionUID = -3040845385309832581L;
	private int row;
	private int col;
	
	public CheckersPosition(int row, int col)
	{
		super();
		
		this.row = row;
		this.col = col;
	}

	public int getRow() {
		return this.row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return this.col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public boolean positionOutOfBounds()
	{
		return this.colOutOfBounds() || this.rowOutOfBounds();
	}
	
	public boolean colOutOfBounds()
	{
		return this.col < 0 || this.col >= CheckersDefaults.DFLT_NR_OF_COLS;
	}
	
	public boolean rowOutOfBounds()
	{
		return this.row < 0 || this.row >= CheckersDefaults.DFLT_NR_OF_ROWS;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + col;
		result = prime * result + row;
		return result;
	}

	@Override
	public String toString()
	{
		return "[ " + this.row + " / " + this.col + " ]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckersPosition other = (CheckersPosition) obj;
		if (col != other.col)
			return false;
		if (row != other.row)
			return false;
		return true;
	}
	
	@Override
	public CheckersPosition clone()
	{
		CheckersPosition theCopy = null;

		try {
			theCopy = (CheckersPosition) super.clone();
			theCopy.setRow(this.row);
			theCopy.setCol(this.col);
		} catch (CloneNotSupportedException ex) {
			ex.printStackTrace();
		}

		return theCopy;
	}
	
}
