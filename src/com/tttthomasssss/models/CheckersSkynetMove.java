package com.tttthomasssss.models;

import com.tttthomasssss.defaults.CheckersUtils;

public class CheckersSkynetMove {
	
	private CheckersMoveSequence moveSequence;
	private boolean isCapture;
	private boolean canPromoteToKing;
	private CheckersBoardState currentState;
	private int whitePiecesLeft;
	private int blackPiecesLeft;
	
	public CheckersSkynetMove(CheckersBoardState state)
	{
		super();
		
		this.currentState = state;
		this.moveSequence = this.currentState.getMoveSequence();
		this.isCapture = this.moveSequence.isFirstMoveCapture();
		//this.canPromoteToKing = CheckersUtils.canPromoteToKingAllowRedundant(this.moveSequence.peekLastPosition(), this.currentState);	
		this.canPromoteToKing = this.moveSequence.isKingPromotionMove();
		this.whitePiecesLeft = state.getWhitePiecesLeft();
		this.blackPiecesLeft = state.getBlackPiecesLeft();
		
	}
	
	public int getWhitePiecesLeft()
	{
		return this.whitePiecesLeft;
	}
	
	public int getBlackPiecesLeft()
	{
		return this.blackPiecesLeft;
	}
	
	public CheckersMoveSequence getMoveSequence()
	{
		return this.moveSequence;
	}
	
	public boolean isCapture()
	{
		return this.isCapture;
	}
	
	public boolean canPromoteToKing()
	{
		return this.canPromoteToKing;
	}
	
	@Override
	public String toString()
	{
		return "isCapture=[" + this.isCapture + "]; canPromoteToKing=[" + this.canPromoteToKing + "]; moveSequence=[" + this.moveSequence + "]";
	}
	
}
