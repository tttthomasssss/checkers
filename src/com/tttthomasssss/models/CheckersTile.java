package com.tttthomasssss.models;

import java.io.Serializable;

public class CheckersTile 
implements Cloneable, Serializable
{
	private static final long serialVersionUID = 2342272889012722254L;
	private CheckersPiece piece;
	private CheckersPosition position;
	
	public CheckersTile()
	{
		this(null);
	}
	
	public CheckersTile(CheckersPiece piece)
	{
		super();
		this.piece = piece;
	}
	
	public CheckersPiece getPiece()
	{
		return this.piece;
	}
	
	public void setPiece(CheckersPiece piece)
	{
		this.piece = piece;
	}
	
	public String toString()
	{
		String str = "";
		
		if (this.piece == null) {
			str = "  ";
		} else {
			str = this.piece.toString();
		}
		return "[" + str + "]";
	}
	
	public CheckersTile clone()
	{
		CheckersTile theCopy = null;
		
		try {
			theCopy = (CheckersTile) super.clone();
			if (this.piece != null) {
				theCopy.piece = this.piece.clone();
			}
		} catch (CloneNotSupportedException ex) {
			ex.printStackTrace();
		}
		
		return theCopy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((piece == null) ? 0 : piece.hashCode());
		result = prime * result
				+ ((position == null) ? 0 : position.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckersTile other = (CheckersTile) obj;
		if (piece == null) {
			if (other.piece != null)
				return false;
		} else if (!piece.equals(other.piece))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		return true;
	}
	
	
}
