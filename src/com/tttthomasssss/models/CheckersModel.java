package com.tttthomasssss.models;

import java.io.Serializable;
import java.util.ArrayList;

import com.tttthomasssss.controllers.CheckersController;
import com.tttthomasssss.defaults.CheckersDefaults;
import com.tttthomasssss.defaults.CheckersDefaults.PieceColour;
import com.tttthomasssss.defaults.CheckersUtils;
import com.tttthomasssss.interfaces.CheckersBoardStateDelegate;

public class CheckersModel 
implements CheckersBoardStateDelegate, Serializable {
	
	private static final long serialVersionUID = -340615380594965647L;
	private CheckersController controller;
	private CheckersBoardState currentState;
	
	private PieceColour playerColour;
	
	private int exploreCount;
	
	private int maxSearchDepth;
	
	public CheckersModel(CheckersController controller, PieceColour playerColour)
	{
		this(controller, new CheckersBoardState(playerColour), playerColour);
	}
	
	public CheckersModel(CheckersController controller, CheckersBoardState state, PieceColour playerColour)
	{
		super();
		this.controller = controller;
		this.currentState = state;
		this.currentState.setDelegate(this);
		this.playerColour = playerColour;
		this.exploreCount = 0;
		this.maxSearchDepth = CheckersDefaults.MINIMAX_DEPTH_CUTOFF;
		
		//this.currentState.getSuccessors(playerColour);
	}
	
	public void resetState(PieceColour playerColour)
	{
		this.playerColour = playerColour;
		this.currentState = new CheckersBoardState(this.playerColour);
		this.currentState.setDelegate(this);
		this.exploreCount = 0;
		this.maxSearchDepth = CheckersDefaults.MINIMAX_DEPTH_CUTOFF;
	}
	
	public int getExploreCount()
	{
		return this.exploreCount;
	}
	
	public void incrementExploreCount()
	{
		this.exploreCount++;
	}
	
	public void resetExploreCount()
	{
		this.exploreCount = 0;
	}
	
	public void setMaxSearchDepth(int maxDepth)
	{
		this.maxSearchDepth = maxDepth;
	}
	
	public int getMaxSearchDepth()
	{
		return this.maxSearchDepth;
	}
	
	public void setPlayerColour(PieceColour playerColour)
	{
		this.playerColour = playerColour;
	}
	
	public boolean hasPlayerOpenCapturesFromPosition(CheckersPosition pos)
	{
		return this.currentState.hasPlayerOpenCapturesFromPosition(pos);
	}
	
	public boolean movePiece(CheckersPosition from, CheckersPosition to, boolean forceCaptures)
	{	
		boolean valid = false;
		CheckersBoardState newState = null;
		
		// TODO: Check for Multi Jumps HERE!!!!!
		boolean isMulti = false;
		newState = this.currentState.movePiece(from, to, CheckersUtils.isCapture(from, to), isMulti, forceCaptures);
		
		if (newState != null) {
			this.currentState = newState;
			this.currentState.setDelegate(this);
			this.currentState.resetMoveSequence();
			valid = true;
		}
		
		return valid;
	}
	
	public int alphaEvaluation(PieceColour currPlayer, CheckersBoardState state, int currDepth, int alpha, boolean forceCaptures)
	{
		int score = alpha;
		
		this.exploreCount++;
		
		int negSwitch = (currPlayer == this.playerColour) ? 1 : -1;
		
		if (this.isTerminalState(state)) {
			score = state.getScoreForColour(this.playerColour) * negSwitch;
			//bestMove = new CheckersMoveEvaluation(eval);
		} else if (currDepth > this.maxSearchDepth/*CheckersDefaults.MINIMAX_DEPTH_CUTOFF*/) {
			score = state.getScoreForColour(this.playerColour) * negSwitch;
			//bestMove = new CheckersMoveEvaluation(eval);
		} else {
			ArrayList<CheckersBoardState> successors = state.getSuccessors(PieceColour.flipColour(currPlayer), forceCaptures);
			
			if (successors.size() <= 0) {
				score = state.getScoreForColour(this.playerColour) * negSwitch;
				//bestMove = new CheckersMoveEvaluation(eval);
			} else {
				currPlayer = PieceColour.flipColour(currPlayer);
				negSwitch = -negSwitch;
				
				// This is new!!!!
				alpha = -alpha;
				
				for (CheckersBoardState s : successors) {
					
					s.resetMoveSequence();
					int eval = alphaEvaluation(currPlayer, s, currDepth + 1, alpha, forceCaptures);
					
					s.setEvaluation(eval);
					
					if (s.getEvaluation() > state.getEvaluation()) {
						state.setEvaluation(eval);
						score = eval;
						alpha = -eval;
						//alpha = eval;
						
					}
					
					if (-eval > alpha) {
						break;
					}
					
				}
				score = -score;
			}
		}
		return score;
	}
	
	// TODO: Get rid of the negSwitch and only maintain 1 score ("gameScore") on the state which is either positive or negative (state should take care of it)
	// TODO: If only score is being maintained, get rid of the CheckersMoveEvaluation object
	public int alphabetaEvaluation(PieceColour currPlayer, CheckersBoardState state, int currDepth, int alpha, int beta, boolean forceCaptures)
	{
		//CheckersMoveEvaluation bestMove = new CheckersMoveEvaluation(alpha);
		int score = alpha;
		this.exploreCount++;
		
		int negSwitch = (currPlayer == this.playerColour) ? 1 : -1;
		
		if (this.isTerminalState(state)) {
			score = state.getScoreForColour(this.playerColour) * negSwitch;
			//bestMove = new CheckersMoveEvaluation(eval);
		} else if (currDepth > this.maxSearchDepth/*CheckersDefaults.MINIMAX_DEPTH_CUTOFF*/) {
			score = state.getScoreForColour(this.playerColour) * negSwitch;
			//bestMove = new CheckersMoveEvaluation(eval);
		} else {
			ArrayList<CheckersBoardState> successors = state.getSuccessors(PieceColour.flipColour(currPlayer), forceCaptures);
			
			if (successors.size() <= 0) {
				score = state.getScoreForColour(this.playerColour) * negSwitch;
				//bestMove = new CheckersMoveEvaluation(eval);
			} else {
				currPlayer = PieceColour.flipColour(currPlayer);
				negSwitch = -negSwitch;
				
				for (CheckersBoardState s : successors) {
					s.resetMoveSequence();
					int eval = alphabetaEvaluation(currPlayer, s, currDepth + 1, -beta, -alpha, forceCaptures);
					
					s.setEvaluation(eval);
					
					if (s.getEvaluation() > state.getEvaluation()) {
						state.setEvaluation(eval);
						score = eval;
						//alpha = -eval;
						
					}
					
					alpha = (-eval > alpha) ? -eval : alpha;
					
					if (alpha >= beta) {
						break;
						//return -alpha;
						//return score * negSwitch;
						//return -score;
						//return alpha;
						//return state.getScoreForColour(this.playerColour) * negSwitch;
						//System.out.println("PRUNING HERE; RETURNING ALPHA="+alpha);
					}
				}
				score = -score;
			}
		}
		return score;
	}
	
	public int minimaxEvaluation2(PieceColour currPlayer, CheckersBoardState state, int currDepth, boolean forceCaptures)
	{
		int score = 0;
		
		this.exploreCount++;
		
		if (this.isTerminalState(state)) {
			score = state.getScoreForColour(this.playerColour);
		} else if (currDepth > this.maxSearchDepth) {
			score = state.getScoreForColour(this.playerColour);
		} else {
			ArrayList<CheckersBoardState> successors = state.getSuccessors(PieceColour.flipColour(currPlayer), forceCaptures);
			
			if (successors.size() <= 0) {
				score = state.getScoreForColour(this.playerColour);
			} else {
				
				currPlayer = PieceColour.flipColour(currPlayer);
				boolean maximise = currPlayer == this.playerColour;
				
				for (CheckersBoardState s : successors) {
					s.resetMoveSequence();
					
					int eval = this.minimaxEvaluation2(currPlayer, s, currDepth + 1, forceCaptures);
					
					s.setEvaluation(eval);
					
					// miniMAX
					if (maximise) {
						if (!state.isEvalSet() || eval > state.getEvaluation()) {
							state.setEvaluation(eval);
							score = eval;
						}
					// MINImax
					} else {
						if (!state.isEvalSet() || eval < state.getEvaluation()) {
							state.setEvaluation(eval);
							score = eval;
						}
					}
				}
			}
		}
		
		return score;
	}
	
	// TODO: Probably do all of the AI stuff in its own class
	public int minimaxEvaluation(PieceColour currPlayer, CheckersBoardState state, int currDepth, boolean forceCaptures)
	{
		int score = 0;
		
		this.exploreCount++;
		
		int negSwitch = (currPlayer == this.playerColour) ? 1 : -1;
		
		// Check if the current state is a terminal state (= either player has won the game)
		if (this.isTerminalState(state)) {
			score = state.getScoreForColour(this.playerColour) * negSwitch;
		} else if (currDepth > this.maxSearchDepth/*CheckersDefaults.MINIMAX_DEPTH_CUTOFF*/) {
			score = state.getScoreForColour(this.playerColour) * negSwitch;//state.getScoreForColour(currPlayer);
		} else {
			
			ArrayList<CheckersBoardState> successors = state.getSuccessors(PieceColour.flipColour(currPlayer), forceCaptures);
			
			if (successors.size() <= 0) {
				score = state.getScoreForColour(this.playerColour) * negSwitch;//state.getScoreForColour(currPlayer);
			} else {
				currPlayer = PieceColour.flipColour(currPlayer);
				negSwitch = -negSwitch;
				for (CheckersBoardState s : successors) {
					s.resetMoveSequence();
					int eval = minimaxEvaluation(currPlayer, s, currDepth + 1, forceCaptures);
					
					s.setEvaluation(eval);
					if (s.getEvaluation() > state.getEvaluation()) {
						state.setEvaluation(eval);
						score = eval;
					}
				}
				score = -score; // I can has NegMax???!!!
			}
		}
		
		return score;
	}
	
	public CheckersController getController() 
	{
		return controller;
	}

	public void setController(CheckersController controller) 
	{
		this.controller = controller;
	}

	public CheckersBoardState getCurrentState() 
	{
		return currentState;
	}
	
	public void printCurrentState()
	{
		this.currentState.printGameBoard();
	}
	
	public void printCurrentScore()
	{
		System.out.println("CURRENT GAME SCORE: "+ this.currentState.getGameScore());
	}
	
	public boolean isTerminalState(CheckersBoardState state)
	{
		return state.isWinningStateForColour(this.playerColour) || state.isWinningStateForColour(PieceColour.flipColour(this.playerColour));
	}
	
	public boolean isCurrentStateTerminalState()
	{
		return this.isTerminalState(this.currentState);
	}
	
	public boolean hasPlayerWon()
	{
		return this.currentState.isWinningStateForColour(this.playerColour);
	}
	
	public boolean hasSkynetWon()
	{
		return this.currentState.isWinningStateForColour(PieceColour.flipColour(this.playerColour));
	}
	
	public boolean hasPlayerMoreMoves(boolean forceCaptures)
	{
		ArrayList<CheckersBoardState> immediateSuccessorsForPlayer = this.currentState.getSuccessors(this.playerColour, forceCaptures);
		
		return immediateSuccessorsForPlayer.size() > 0;
	}
	
	public CheckersSkynetMove runSkynet(boolean alphaBetaPruning, boolean forceCaptures)
	{
		CheckersBoardState state = this.getCurrentState();
		ArrayList<CheckersBoardState> bestStates = new ArrayList<CheckersBoardState>();
		//int alpha = Integer.MIN_VALUE + 1;
		int alpha = Integer.MAX_VALUE;
		
		ArrayList<CheckersBoardState> successors = state.getSuccessors(PieceColour.flipColour(this.playerColour), forceCaptures);
		int max = Integer.MIN_VALUE;
		
		for (CheckersBoardState s : successors) {
			//s.resetMoveSequence();
			//int eval = (alphaBetaPruning) ? this.alphabetaEvaluation(PieceColour.flipColour(this.playerColour), s, 1, -beta, -alpha, forceCaptures) : this.minimaxEvaluation(PieceColour.flipColour(this.playerColour), s, 1, forceCaptures);
			//int eval = (alphaBetaPruning) ? this.alphaEvaluation(PieceColour.flipColour(this.playerColour), s, 1, -alpha, forceCaptures) : this.minimaxEvaluation(PieceColour.flipColour(this.playerColour), s, 1, forceCaptures);
			int eval = (alphaBetaPruning) ? this.alphaEvaluation(PieceColour.flipColour(this.playerColour), s, 1, alpha, forceCaptures) : this.minimaxEvaluation(PieceColour.flipColour(this.playerColour), s, 1, forceCaptures);
			
			if (eval == max) {
				bestStates.add(s);
			} else if (eval > max) {
				bestStates.clear();
				bestStates.add(s);
				max = eval;
				alpha = -eval;
				//alpha = eval;
			}
			
			// Alpha-Beta Pruning bookkeeping
			////beta = eval; // TODO: Check if this is really doing it!!!
			//alpha = -eval;
		}
		
		this.controller.updateExploreCount(this.exploreCount);
		this.resetExploreCount();
		
		// Move Selection
		CheckersSkynetMove move = null;
		if (successors.size() > 0) {
			int index = (int)(Math.random() * bestStates.size());
			state = bestStates.get(index);
			state.printGameBoard();

			this.currentState = state;
			this.currentState.setDelegate(this);
			
			move = new CheckersSkynetMove(this.currentState);
		}
		
		return move;
	}
	
	public CheckersMoveSequence generateHint(PieceColour colour, boolean alphaBetaPruning, boolean forceCaptures)
	{
		CheckersBoardState state = this.getCurrentState();
		ArrayList<CheckersBoardState> bestStates = new ArrayList<CheckersBoardState>();
		int alpha = Integer.MIN_VALUE + 1;
		int beta = Integer.MAX_VALUE;
		
		ArrayList<CheckersBoardState> successors = state.getSuccessors(colour, forceCaptures);
		int max = Integer.MIN_VALUE;
		
		for (CheckersBoardState s : successors) {
			//s.resetMoveSequence();
			//int eval = (alphaBetaPruning) ? this.alphabetaEvaluation(colour, s, 1, -beta, -alpha, forceCaptures) : this.minimaxEvaluation(colour, s, 1, forceCaptures);
			int eval = (alphaBetaPruning) ? this.alphaEvaluation(colour, s, 1, -alpha, forceCaptures) : this.minimaxEvaluation(colour, s, 1, forceCaptures);
			if (eval == max) {
				bestStates.add(s);
			} else if (eval > max) {
				bestStates.clear();
				bestStates.add(s);
				max = eval;
			}
			
			// Alpha-Beta Pruning bookkeeping
			beta = eval; // TODO: Check if this is really doing it!!!
		}
		
		if (successors.size() <= 0) {
			System.out.println("Cant create any moves!!!");
		} else {
			int index = (int)(Math.random() * bestStates.size());
			state = bestStates.get(index);
		}
		
		return state.getMoveSequence();
	}
	
	@Override
	public void capturePieceAt(CheckersPosition pos) 
	{
		this.controller.capturePieceViewAt(pos);
		
	}

	@Override
	public void promotePieceToKingForMove(CheckersPosition from, CheckersPosition to) 
	{
		this.controller.promotePieceViewToKingForMove(from, to);
	}
	
	@Override
	public void updatePieceCount(int whitePiecesLeft, int blackPiecesLeft)
	{
		this.controller.updatePieceCount(whitePiecesLeft, blackPiecesLeft);
	}
}
