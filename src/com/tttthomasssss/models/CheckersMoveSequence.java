package com.tttthomasssss.models;

import java.io.Serializable;
import java.util.*;

import com.tttthomasssss.controllers.ValidationController;
import com.tttthomasssss.defaults.CheckersUtils;

public class CheckersMoveSequence 
implements Cloneable, Serializable
{
	
	// TODO: If time, implement as Factory
	
	private static final long serialVersionUID = 2973437700232273130L;
	private Deque<CheckersPosition> moveSequence;
	private ValidationController validationController;
	private boolean kingPromotionMove;
	
	public CheckersMoveSequence(ValidationController validationController)
	{
		super();
		this.moveSequence = new ArrayDeque<CheckersPosition>();
		this.validationController = validationController;
		this.kingPromotionMove = false;
	}
	
	public Deque<CheckersPosition> getMoveSequence()
	{
		return this.moveSequence;
	}
	
	public boolean isEmpty()
	{
		return this.moveSequence.isEmpty();
	}
	
	public boolean isFirstMoveCapture()
	{
		boolean capture = false;
		if (this.hasMoreMoves()) {
			
			// Dequeue Positions
			CheckersPosition from = this.moveSequence.removeFirst();
			CheckersPosition to = this.moveSequence.removeFirst();
			
			// Check
			capture = CheckersUtils.isCapture(from, to);
			
			// Enqueue Positions again
			this.moveSequence.addFirst(to);
			this.moveSequence.addFirst(from);
		}
		
		return capture;
	}
	
	public boolean hasMoreMoves()
	{
		return this.moveSequence.size() >= 2;
	}
	
	public CheckersPosition popFirst()
	{
		return this.moveSequence.removeFirst();
	}
	
	public CheckersPosition peekFirst()
	{
		return this.moveSequence.getFirst();
	}
	
	public boolean addMove(CheckersPosition moveFrom, CheckersPosition moveTo, CheckersBoardState currBoardState)
	{
		boolean validMove = false;
		
		// If the Queue is empty, initialise it with moveFrom
		if (this.moveSequence.isEmpty()) {
			this.moveSequence.add(moveFrom);
		}
		
		// Check if the last move in the queue is identical with moveFrom and the move is valid generally
		if (this.moveSequence.peekLast().equals(moveFrom) && this.validationController.validMove(moveFrom, moveTo, currBoardState)) {
			this.moveSequence.addLast(moveTo);
			validMove = true;
			
			// Possibly use the kingPromotionMove var to check whether adding another move is allowed
			if (this.checkKingPromotionMove(moveFrom, moveTo, currBoardState)) {
				this.kingPromotionMove = true;
			}
		} 
		
		return validMove;
	}
	
	public boolean isKingPromotionMove()
	{
		return this.kingPromotionMove;
	}
	
	public CheckersPosition peekLastPosition()
	{
		return this.moveSequence.peekLast();
	}
	
	public CheckersPosition peekFirstPosition()
	{
		return this.moveSequence.peekFirst();
	}
	
	@Override
	public String toString()
	{
		String str = "";
		for (CheckersPosition pos : this.moveSequence) {
			str += (pos.toString() + " --> ");
		}
		int idx = str.lastIndexOf(" --> ");
		
		if (idx > 0) {
			str = str.substring(0, idx);
		}
		return str;
	}
	
	@Override
	public CheckersMoveSequence clone()
	{
		CheckersMoveSequence theCopy = null;
		
		try {
			theCopy = (CheckersMoveSequence) super.clone();
			theCopy.moveSequence = new ArrayDeque<CheckersPosition>(this.moveSequence.size());
			for (CheckersPosition pos : this.moveSequence) {
				theCopy.moveSequence.add(pos.clone());
			}
			
		} catch (CloneNotSupportedException ex) {
			ex.printStackTrace();
		}
		
		return theCopy;
	}
	
	private boolean checkKingPromotionMove(CheckersPosition from, CheckersPosition to, CheckersBoardState state)
	{	
		boolean whiteKingPromotion = (to.getRow() == 0 && state.isWhiteAt(from));
		boolean blackKingPromotion = (to.getRow() == state.getRows() - 1 && !state.isWhiteAt(from));
			
		return whiteKingPromotion || blackKingPromotion;
	}
}
