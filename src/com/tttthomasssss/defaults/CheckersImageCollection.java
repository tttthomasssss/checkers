package com.tttthomasssss.defaults;

import javax.swing.ImageIcon;

import com.tttthomasssss.defaults.CheckersDefaults.PieceColour;
import com.tttthomasssss.defaults.CheckersDefaults.PieceIdentity;

public class CheckersImageCollection 
{
	
	private static final 	ClassLoader 			LOADER = CheckersImageCollection.class.getClassLoader();
	private static 			CheckersImageCollection INSTANCE = null;
	
	// Game Icons
	private ImageIcon blackPieceIcon;
	private ImageIcon blackKingIcon;
	private ImageIcon whitePieceIcon;
	private ImageIcon whiteKingIcon;
	
	// Tutorial Icons
	private ImageIcon checkersBoardIcon;
	private ImageIcon checkersPiecesIcon;
	private ImageIcon checkersBoardStartingPositionIcon;
	private ImageIcon checkersSingleMoveIcon;
	private ImageIcon checkersCaptureIcon;
	private ImageIcon checkersMultiCaptureIcon;
	private ImageIcon checkersKingsIcon;
	
	
	private CheckersImageCollection()
	{
		super();
		
		this.blackPieceIcon						= new ImageIcon(LOADER.getResource("blackPiece.png"));
		this.blackKingIcon 						= new ImageIcon(LOADER.getResource("blackKing.png"));
		this.whitePieceIcon						= new ImageIcon(LOADER.getResource("whitePiece.png"));
		this.whiteKingIcon						= new ImageIcon(LOADER.getResource("whiteKing.png"));
		this.checkersBoardIcon 					= new ImageIcon(LOADER.getResource("emptyBoard.png"));
		this.checkersPiecesIcon 				= new ImageIcon(LOADER.getResource("checkersPieces.png"));
		this.checkersBoardStartingPositionIcon 	= new ImageIcon(LOADER.getResource("startingPosBoard.png"));
		this.checkersSingleMoveIcon				= new ImageIcon(LOADER.getResource("singleMove.png"));
		this.checkersCaptureIcon				= new ImageIcon(LOADER.getResource("capture.png"));
		this.checkersMultiCaptureIcon			= new ImageIcon(LOADER.getResource("multiCapture.png"));
		this.checkersKingsIcon					= new ImageIcon(LOADER.getResource("checkersKings.png"));
		
	}
	
	public ImageIcon getBlackPieceIcon() 
	{
		return this.blackPieceIcon;
	}

	public ImageIcon getBlackKingIcon() 
	{
		return this.blackKingIcon;
	}

	public ImageIcon getWhitePieceIcon() 
	{
		return this.whitePieceIcon;
	}

	public ImageIcon getWhiteKingIcon() 
	{
		return this.whiteKingIcon;
	}
	
	public ImageIcon getCheckersBoardIcon() {
		return checkersBoardIcon;
	}

	public ImageIcon getCheckersPiecesIcon() {
		return checkersPiecesIcon;
	}

	public ImageIcon getCheckersKingsIcon() {
		return checkersKingsIcon;
	}
	
	public ImageIcon getCheckersBoardStartingPositionIcon() {
		return checkersBoardStartingPositionIcon;
	}

	public ImageIcon getCheckersSingleMoveIcon() {
		return checkersSingleMoveIcon;
	}

	public ImageIcon getCheckersCaptureIcon() {
		return checkersCaptureIcon;
	}

	public ImageIcon getCheckersMultiCaptureIcon() {
		return checkersMultiCaptureIcon;
	}

	public ImageIcon getKingIconForColour(PieceColour colour)
	{
		return (colour == PieceColour.WHITE) ? this.getWhiteKingIcon() : this.getBlackKingIcon();
	}
	
	public ImageIcon getPieceIconForColour(PieceColour colour)
	{
		return (colour == PieceColour.WHITE) ? this.getWhitePieceIcon() : this.getBlackPieceIcon();
	}
	
	public ImageIcon getPieceIconForColourAndIdentity(PieceColour colour, PieceIdentity identity)
	{
		return (identity == PieceIdentity.NORMAL) ? this.getPieceIconForColour(colour) : this.getKingIconForColour(colour);
	}
	
	public static CheckersImageCollection getInstance()
	{
		if (CheckersImageCollection.INSTANCE == null) {
			CheckersImageCollection.INSTANCE = new CheckersImageCollection();
		}
		
		return CheckersImageCollection.INSTANCE;
	}
	
}
