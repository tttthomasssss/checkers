package com.tttthomasssss.defaults;

public class CheckersDefaults 
{
	
	public static final int DFLT_NR_OF_ROWS 		= 8;
	public static final int DFLT_NR_OF_COLS 		= 8;
	public static final int DFLT_NR_OF_PIECES 		= 12;
	public static final int WINNING_STATE 			= 100000;
	public static final int LOSING_STATE			= 0;
	public static final int LOSING_STATE_MINIMAX	= -100000;
	public static final int KING_BONUS				= 2;
	public static final int CAPTURE_BONUS			= 5;
	public static final int MINIMAX_DEPTH_CUTOFF	= 15;//1;//2;//7;
	public static final int DFLT_TIMER_DELAY		= 500;
	
	public static final int R_DARK_TILE_COLOUR	= 156;
	public static final int G_DARK_TILE_COLOUR	= 89;
	public static final int B_DARK_TILE_COLOUR	= 32;
	
	public static final int R_LIGHT_TILE_COLOUR	= 233;
	public static final int G_LIGHT_TILE_COLOUR	= 212;
	public static final int B_LIGHT_TILE_COLOUR	= 179;
	
	public static final String PREFS_FORCE_CAPTURES_KEY		= "forceCaptures";
	public static final String PREFS_PLAYER_COLOUR_KEY		= "playerColour";
	public static final String PREFS_DIFFICULTY_KEY			= "difficulty";
	public static final String PREFS_ALPHA_BETA_PRUNING_KEY	= "alphaBetaPruning";
	
	public static final boolean	PREFS_FORCE_CAPTURES_DEFAULT_VALUE		= true;
	public static final int		PREFS_PLAYER_COLOUR_DEFAULT_VALUE		= 0;
	public static final int 	PREFS_DIFFICULTY_DEFAULT_VALUE			= 3;
	public static final boolean PREFS_ALPHA_BETA_PRUNING_DEFAULT_VALUE	= true;
	
	public static final int DIFFICULTY_MIN_VALUE = 1;
	public static final int DIFFICULTY_MAX_VALUE = 5;
	
	public enum PieceColour
	{
		// White
		WHITE(0),
		
		BLACK(1);
		
		private int value;
		
		PieceColour(int value)
		{
			this.value = value;
		}
		
		public int getValue()
		{
			return this.value;
		}
		
		public static PieceColour colourForValue(int value)
		{
			return (value == 0) ? PieceColour.WHITE : PieceColour.BLACK;
		}
		
		public static PieceColour flipColour(PieceColour colour)
		{
			return (colour == PieceColour.WHITE) ? PieceColour.BLACK : PieceColour.WHITE;
		}
		
		public String toString()
		{
			return (this == PieceColour.WHITE) ? "White" : "Black";
		}
	}
	
	public enum PieceIdentity
	{
		// Normal Piece
		NORMAL(0),
		
		// King
		KING(1);
		
		private int value;
		
		PieceIdentity(int value)
		{
			this.value = value;
		}
		
		public int getValue()
		{
			return this.value;
		}
		
		public String toString()
		{
			return (this == PieceIdentity.NORMAL) ? "Normal" : "King";
		}
	}
}
