package com.tttthomasssss.defaults;

import java.awt.Color;

import com.tttthomasssss.defaults.CheckersDefaults.PieceColour;
import com.tttthomasssss.models.CheckersBoardState;
import com.tttthomasssss.models.CheckersPosition;

public class CheckersUtils 
{
	
	public static boolean isCapture(CheckersPosition from, CheckersPosition to)
	{
		return (Math.abs(from.getRow() - to.getRow()) > 1 && Math.abs(from.getCol() - to.getCol()) > 1);
	}
	
	public static Color getDarkTileColour()
	{
		return new Color(CheckersDefaults.R_DARK_TILE_COLOUR, CheckersDefaults.G_DARK_TILE_COLOUR, CheckersDefaults.B_DARK_TILE_COLOUR);
	}
	
	public static Color getLightTileColour()
	{
		return new Color(CheckersDefaults.R_LIGHT_TILE_COLOUR, CheckersDefaults.G_LIGHT_TILE_COLOUR, CheckersDefaults.B_LIGHT_TILE_COLOUR);
	}
	
	public static CheckersPosition getCapturedPiecePosition(CheckersPosition from, CheckersPosition to)
	{
		int enemyRow = (from.getRow() > to.getRow()) ? from.getRow() - 1 : from.getRow() + 1;
		int enemyCol = (from.getCol() > to.getCol()) ? from.getCol() - 1 : from.getCol() + 1;
		
		return new CheckersPosition(enemyRow, enemyCol);
	}
	
	public static boolean canPromoteToKingAllowRedundant(CheckersPosition to, CheckersBoardState boardState)
	{
		boolean whiteKingPromotion = (to.getRow() == 0 && boardState.isWhiteAt(to));
		boolean blackKingPromotion = (to.getRow() == boardState.getRows() - 1 && !boardState.isWhiteAt(to));
		
		return (whiteKingPromotion || blackKingPromotion);
	}
	
	public static boolean canPromoteToKing(CheckersPosition to, CheckersBoardState boardState) 
	{
		boolean whiteKingPromotion = (to.getRow() == 0 && boardState.isWhiteAt(to) && !boardState.isKingAt(to));
		boolean blackKingPromotion = (to.getRow() == boardState.getRows() - 1 && !boardState.isWhiteAt(to) && !boardState.isKingAt(to));
		
		return (whiteKingPromotion || blackKingPromotion);
	}
	
	public static PieceColour[] getGameColours() 
	{
		PieceColour[] gameColours = {PieceColour.WHITE, PieceColour.BLACK};
		
		return gameColours;
	}
	
	public static int sanitiseDifficulty(int difficulty)
	{
		if (difficulty < 0 || difficulty > 5) {
			difficulty = CheckersDefaults.PREFS_DIFFICULTY_DEFAULT_VALUE;
		}
		
		return difficulty;
	}
	
	public static int getMaxSearchDepthForDifficultyAndPruningOption(int difficulty, boolean alphaBetaPruning)
	{
		// You can just never do it often enough...
		difficulty = CheckersUtils.sanitiseDifficulty(difficulty);
		
		int maxSearchDepth = 0;
		
		if (difficulty > 1) {
			maxSearchDepth = alphaBetaPruning ? Math.min(difficulty * 2, 9) : difficulty + 1;
		}
		
		return maxSearchDepth;
	}
}
